package com.tradavo.utils.etailer;

import com.opencsv.CSVReader;
import com.tradavo.utils.App;
import com.tradavo.utils.database.DBConnector;
import com.tradavo.utils.lineitems.etailer.EtailerItemLine;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 */
public class EtailerLoaderApp
{
	public static Logger logger = LogManager.getLogger(EtailerLoaderApp.class) ;
	public static Logger dLog = LogManager.getLogger("descriptionRef");
	public static int updateCount = 0 ;
	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static int found = 0 ;
	public static int missing = 0 ;
	public static int etinWrite = 0 ;
	private static String[] WIdistros = {"td","tb","tf","tr"} ;
	private static boolean verifyOnly = false;
	private static int validEtins;
	private static List<String> abandonedEtins = new ArrayList<>();
	private static int totalAsin = 0;

	private static Connection getDbConnectionObject(final String type) throws Exception {
		try {
			App.log("Using " + type + " database connection.") ;
			return DBConnector.getConnection(type) ;
		} catch (SQLException ex) {
		    // handle any errors
			throw new Exception ( ex);
		}
	}
	public static void dlog(final String asin,final String distro, String etailer, String merchant){
		//etailer = etailer.replaceAll(",","[com]");
		//merchant = merchant.replaceAll(",","[com]");
		dLog.log(Level.INFO, asin+"|"+distro+"|"+etailer+"|"+merchant);
	}
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static void parseFile(String fileName, Connection conn) throws Exception {
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);
		try {
			// set up the first field in the csv file
			EtailerLoaderApp.dlog("ASIN", "DISTRO","ETAILER","merchant");
		CSVReader reader = new CSVReader(new FileReader(fileName), '\t');
		EtailerLoaderApp.log("Processing product load file " + fileName);
		//List<Object> list = new ArrayList<>();
		//try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName),Charset.forName("Cp1252"))) {
			// br returns as stream and convert it into a List
			reader.readNext();
			int index = 1;
			String[] record = null;
			while((record = reader.readNext()) != null){
				try {
					if( EtailerLoaderApp.testCount > 0 && index > EtailerLoaderApp.testCount )
						break ;
					EtailerLoaderApp.log ( "Record:"+ index);// a separator line
					// if( index > 2089 && index < 2091)
						processRow(record, conn);
					index++ ;
				}
				catch ( Exception e) {
					EtailerLoaderApp.log ( "Error Processing " + index + e.getMessage());
				}
			}
			reader.close();
			if(EtailerLoaderApp.verifyOnly){
				EtailerLoaderApp.log("\t\t\t\t ABANDONED ETINS");
				for(String s : abandonedEtins)
					EtailerLoaderApp.log(s);
				EtailerLoaderApp.log("Verification success of " + validEtins);
				EtailerLoaderApp.log("Number of product records " + totalAsin);
			}else {
				EtailerLoaderApp.log("ETIN values written:" + etinWrite);
			}
			EtailerLoaderApp.log("Total products processed:" + --index);
		} catch (IOException e) {
			throw new Exception("IO error when trying to read error count file " + fileName, e);
		} catch (NumberFormatException e) {
			throw new Exception("Unable to parse value in error file.", e);
		}
	}

	private static int runUpdateSQL ( Connection conn, PreparedStatement st, final String etin, final String asinIn,final String distros)  throws SQLException{
		//validate product_group
		ResultSet rs = null;
		int ct = 0;
		try {
			boolean found = false;
			rs = st.executeQuery();
			while (rs.next()) {
				String asinItem = rs.getString("asin");
				String distroItem = rs.getString("distro");
				EtailerLoaderApp.log("ASIN:" + asinItem + "     DISTRO:" + distroItem);
				writeEtin(conn, asinItem, distroItem, etin);
				EtailerLoaderApp.log(validateEtin(conn, etin, distroItem) + " active amazon product record(s) found for " + etin + "/" + distroItem);
				ct++;
				found = true;
			}
			if (!found) {
				EtailerLoaderApp.log("ETIN/ASIN not found\t" + etin + "\t"+asinIn + "\t"+distros) ;
				writeEtin(conn, "%"+asinIn,distros,etin) ;
			}
		}catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();

			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		return ct ;
	}

	private static int productExists ( Connection conn, final String asin, final String asin2, final String distros, final String etin, final String desc)  throws SQLException{
		//validate product_group
		String sql = "Select asin,distro,item_description From product.amazon_product Where (asin = ? or asin = ?) and distro like ? "; // and active_product_count = 1";

		PreparedStatement st = null;
		ResultSet rs = null;
		int ct = 0;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, asin);
			st.setString(2,asin2) ;
			st.setString( 3, distros) ;
			ct = EtailerLoaderApp.runUpdateSQL(conn, st,etin,asin,distros);
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		return ct ;
	}

	private static int productExists ( Connection conn, final String asin, final String distros, final String etin, final String desc)  throws SQLException{
		//validate product_group
		String sql = "Select asin,distro,item_description From product.amazon_product Where asin = ? and distro like ? "; // and active_product_count = 1";

		PreparedStatement st = null;
		ResultSet rs = null;
		int ct = 0;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, asin);
			st.setString(2, distros);
			ct = EtailerLoaderApp.runUpdateSQL(conn, st, etin,asin,distros);
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();

			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		return ct ;
	}
	static final String productUpdate = "Update product.amazon_product set ";

	private static String conCatFieldData(String colName, int colCnt, String colVal, Boolean stringVal) {
		return (colCnt > 0 ? "," : "") + colName + "=" + ( stringVal ? "'" : "") + colVal.replaceAll("'", "''") + ( stringVal ? "'" : ""); 
	}

	private static int validateEtin(Connection conn, final String etin, final String distro) throws SQLException{
		PreparedStatement st = null;
		ResultSet rs = null;
		int ct = 0 ;
		String sql = "select count(asin) thecount from product.amazon_product where vendor_item_id = ? and distro = ? and active_product_count > 0" ;
		try{
			st = conn.prepareStatement(sql);
			st.setString(1, etin);
			st.setString(2, distro);
			rs = st.executeQuery();
			if(rs.next())
				ct = rs.getInt("thecount");
		} catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
		}
		return ct ;
	}

	private static void writeEtin(Connection conn, final String asin, final String distro, final String etin) throws SQLException{
		PreparedStatement st = null;
		try{
			String updateStmt = productUpdate ;
			updateStmt += conCatFieldData("vendor_item_id",0,etin, true);
			//updateStmt += conCatFieldData("item_description", 1,iDesc, true);
			if(asin.contains("%") || distro.contains("%"))
				updateStmt += " where asin like ? and distro like ?" ;
			else
				updateStmt += " where asin = ? and distro = ?" ;
			st = conn.prepareStatement(updateStmt);
			st.setString(1, asin);
			st.setString(2, distro) ;
			etinWrite += writeData(st);
			EtailerLoaderApp.log("DB Write \t" + etin + "\t" + asin + "\t" + distro );
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			EtailerLoaderApp.cleanup(st,null);
		}
	}

	private static void cleanup(PreparedStatement s, ResultSet r) throws SQLException{
		if ( s != null && !s.isClosed())
			s.close();
		if ( r != null && !r.isClosed())
			r.close();
	}
    private static void processRow(String[] lineCols, Connection conn)  throws Exception{
		try
		{
			EtailerItemLine item = new EtailerItemLine(lineCols);
			EtailerLoaderApp.log(item.getId());
			if ( item.etin == null || item.etin.trim().isEmpty() ){
				EtailerLoaderApp.error ( "ETIN is null or empty. All these values are required to insert or update a product.");
				return;
			}
			if("ETFZ-0000-0370".equals(item.etin)){
				EtailerLoaderApp.log("\t\t\t"+ item.etin + " why does this one get skipped???");
			}
			if( EtailerLoaderApp.verifyOnly ){
				EtailerLoaderApp.verify(item,conn);
				//EtailerLoaderApp.log(item.getItemId());
				EtailerLoaderApp.log(item.getAsinCount());
				if(item.asinCount > 0) {
					EtailerLoaderApp.validEtins++;
					EtailerLoaderApp.totalAsin+= item.asinCount;
				}
				return;  // verify only is really just verify only
			}
			String prefix = item.getPrefix() ;
			// EtailerLoaderApp.log("Etin prefix is " + prefix);
			String prefixAsin = item.asinWithPrefix(item.warehouseWI);
			if(!item.isFieldEmpty(item.warehouseWI)) {
				for( String ss : WIdistros){
					prefixAsin = (item.warehouseWI.equalsIgnoreCase(prefixAsin) ? null : prefixAsin);
					int dCt = EtailerLoaderApp.productCounter(conn, item.warehouseWI,prefixAsin, ss, item.etin, item.item_description);
					EtailerLoaderApp.log("\t\t"+item.warehouseWI + " (WI) has " + dCt + " " + ss + " amazon product records.");
				}
			}
			if(!item.isFieldEmpty(item.warehousePA)) {
				prefixAsin = item.asinWithPrefix(item.warehousePA);
				prefixAsin = (item.warehousePA.equalsIgnoreCase(prefixAsin) ? null : prefixAsin);
				EtailerLoaderApp.log("\t\t"+item.warehousePA + " (PA) has " + EtailerLoaderApp.productCounter(conn, item.warehousePA,prefixAsin, "tp%", item.etin, item.item_description) + " TP amazon product records.");
			}
			if(!item.isFieldEmpty(item.warehouseNV)) {
				prefixAsin = item.asinWithPrefix(item.warehouseNV);
				prefixAsin = (item.warehouseNV.equalsIgnoreCase(prefixAsin) ? null : prefixAsin);
				EtailerLoaderApp.log("\t\t"+item.warehouseNV + " (NV) has " + EtailerLoaderApp.productCounter(conn, item.warehouseNV,prefixAsin, "dow%", item.etin, item.item_description) + " DOW amazon product records.");
			}
			conn.setAutoCommit(false);
			//conn.commit();
		}
		catch ( SQLException e) {
			conn.rollback();
			EtailerLoaderApp.error(e.getMessage());
			throw new Exception ( e );
		}
		
	}

	public static int productCounter(Connection conn, String asin,String asin2, String distros, final String etin, final String desc) throws SQLException{
		if(asin2 == null)
			return EtailerLoaderApp.productExists(conn, asin, distros, etin,desc) ;
		return EtailerLoaderApp.productExists(conn, asin,asin2, distros, etin,desc) ;
	}

	public static int writeData(PreparedStatement st) throws SQLException {
	    if( EtailerLoaderApp.writeAllData)
	        return st.executeUpdate() ;
	    else
	    	EtailerLoaderApp.log("No Etailer data written to database.");
	    return 0 ;
    }
    public static final String verifySQL = "SELECT count(asin) " +
				"from product.amazon_product where vendor_item_id = ?;";
    public static void verify(EtailerItemLine item,Connection conn) throws SQLException{
		EtailerLoaderApp.log(item.getItemId());
		PreparedStatement s = conn.prepareStatement(verifySQL);
		s.setString(1,item.etin);
		ResultSet r = s.executeQuery();
		if( r.next() ){
			item.asinCount = r.getInt(1);
		}
		if(item.asinCount == 0)
			EtailerLoaderApp.abandonedEtins.add(item.getValues());
		cleanup(s,r);
	}
    
	public static void main( String[] args )
    {
        if ( args.length < 1) {
        	EtailerLoaderApp.fatal( "Error! Please pass file name to upload");
        	return;
        }
		EtailerLoaderApp.log( "Starting Etailer Product Load processing.");
		Connection conn = null;
		try {
			conn = getDbConnectionObject("staging");
			if( args.length >= 2 && args[1].equals("live"))
			    EtailerLoaderApp.writeAllData = true ;
			else if( args.length >= 2 && args[1].equals("verify"))
				EtailerLoaderApp.verifyOnly = true ;
			else
			    EtailerLoaderApp.log("Not running live, will be doing everything EXCEPT writing data to the DB");
			if( args.length == 3 && !args[2].equals("")){
				EtailerLoaderApp.testCount = Integer.parseInt(args[2]);
			}
			parseFile(args[0], conn);
		} catch (Exception e) {
			EtailerLoaderApp.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			EtailerLoaderApp.log("Completed Etailer product load processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				EtailerLoaderApp.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }
}
