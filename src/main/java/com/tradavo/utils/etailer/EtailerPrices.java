package com.tradavo.utils.etailer;

import com.opencsv.CSVReader;
import com.tradavo.utils.database.DBConnector;
import com.tradavo.utils.lineitems.etailer.EtailerPriceLine;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 *
 */
public class EtailerPrices
{
	public static Logger logger = LogManager.getLogger(EtailerPrices.class) ;
	public static int updateCount = 0 ;
	public static int differenceCount = 0;
	public static int sameCount = 0;
	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static boolean verifyOnly ;
	private static Connection getDbConnectionObject(final String type) throws Exception {
			try {
				EtailerPrices.log("Using " + type + " database connection.") ;
				return DBConnector.getConnection(type) ;
			} catch (SQLException ex) {
				// handle any errors
				throw new Exception ( ex);
			}
	}
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static void parseFile(String fileName, Connection conn) throws Exception {
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);
		try {
		CSVReader reader = new CSVReader(new FileReader(fileName), '\t');
		EtailerPrices.log("Processing product load file " + fileName);
		//List<Object> list = new ArrayList<>();
		//try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName),Charset.forName("Cp1252"))) {
			// br returns as stream and convert it into a List
			reader.readNext();
			int index = 1;
			String[] record = null;
			while((record = reader.readNext()) != null){
				//App.log ( "Record:"+ index);// a separator line
				try {
					if( EtailerPrices.testCount > 0 && index > EtailerPrices.testCount )
						break ;
					if( !verifyOnly )
						processRow(record, conn);
				}
				catch ( Exception e) {
					EtailerPrices.log ( "Error Processing Record " + index + e.getMessage());
				}
				index++ ;
			}
			
			reader.close();
			EtailerPrices.log("Total products processed:" + --index );
			if(!verifyOnly) {
				EtailerPrices.log("Total products updated " + updateCount);
			}else{
				EtailerPrices.log("Price differences " + differenceCount);
				EtailerPrices.log("Prices the same  " + sameCount);
			}
		} catch (IOException e) {
			throw new Exception("IO error when trying to read error count file " + fileName, e);
		} catch (NumberFormatException e) {
			throw new Exception("Unable to parse value in error file.", e);
		}
	}

	private static boolean productExists ( Connection conn, EtailerPriceLine item)  throws SQLException{
		String sql = "Select 1 From product.amazon_product Where distro = ? and asin = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, item.distro);
			st.setString(2, item.asin);
			rs = st.executeQuery();
			if ( !rs.next() ) {
				return false;
			}
			else
				return true;
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}

	private static boolean validateExists ( String sql, Connection conn, String param)  throws SQLException{
		//validate product_group
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, param);
			rs = st.executeQuery();
			if ( !rs.next() ) {
				EtailerPrices.error ( "Invalid  " + param + " for sql " + sql);
				return false;
			}
			else
				return true;
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}


	static final String productUpdate = "Update product.amazon_product set ";
	// static final String productMinimumUpdate = "Update product.product_minimum_record set ";

	static final String getValidationData = "select retail_cost,tradavo_cost, tradavo_cogs from product.amazon_product" +
			" where asin = ? and distro = ? and vendor_item_id = ?" ;
	
	private static String conCatFieldData(String colName, int colCnt, String colVal, Boolean stringVal) {
		return (colCnt > 0 ? "," : "") + colName + "=" + ( stringVal ? "'" : "") + colVal.replaceAll("'", "''") + ( stringVal ? "'" : ""); 
	}

	private static String generateUpdateData( final String colName, final int colCnt, final String[] field,final boolean isString){
		String sql = "";
		if ( field.length > 0) {
			sql = conCatFieldData(colName, colCnt,field[0], isString);
		}
		return sql ;
	}
	private static String generateUpdateData(final String colName, final int colCnt, final String field, final boolean isString){
		String sql = "";
		if ( field.length() > 0) {
			sql = conCatFieldData(colName, colCnt,field, isString);
		}
		return sql ;
	}

	private static void updateProduct(Connection conn, EtailerPriceLine item) throws Exception{
		String sql = productUpdate;

		int colCnt = 0;

		sql +=generateUpdateData(" retail_cost", colCnt, item.retail_cost, false);
		colCnt ++;

		sql +=generateUpdateData(" tradavo_cost", colCnt, item.tradavo_cost, false);
		colCnt ++;

		sql +=  " WHERE asin = '" + item.asin + "' And distro = '" + item.distro + "'";
		if( EtailerPrices.writeAllData)
			EtailerPrices.log("Writing prices for : " + item.getId());
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(sql);
			writeData(st); // .executeUpdate();
		}
		catch ( SQLException e) {
			throw new Exception ( e);
		}
		finally {
			updateCount++ ;
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		
	}

    private static void processRow(String[] lineCols, Connection conn)  throws Exception{
		try
		{
			EtailerPriceLine item = new EtailerPriceLine(lineCols);
			EtailerPrices.log(item.getId());
			String distroSQL = "Select 1 from product.distributors d where d.distro_code = ?";

			if ( ! validateExists(distroSQL, conn, item.distro)) {
				EtailerPrices.error ( "Invalid distro " + item.distro + " for " + item.asin);
				EtailerPrices.error( "A valid distro is required to process this product") ;
				return;
			}

			if ( productExists(conn, item)) {
				// 3-24 this is the test for carts to see if this product is in carts
				findCartEntries( conn, item );  // find cart entries first so we get the old price of the item
				updateProduct(conn, item);
			}else {
				EtailerPrices.log("Not found:"+ item.getId());
			}

			//conn.commit();
		}
		catch ( SQLException e) {
			//conn.rollback();
			EtailerPrices.error(e.getMessage());
			throw new Exception ( e );
		}
	}

	static final String shoppingCartEntries = "select c.account, c.distro,ap.item_description ,c.item, c.price, ap.retail_cost from product.cart c" +
			" join product.amazon_product ap on c.item = ap.asin and c.distro = ap.distro" +
			" where c.distro = ? and c.item = ? ;";

	public static void findCartEntries(Connection conn, EtailerPriceLine item){
		return;
		/*String sql = shoppingCartEntries ;
		int cartCount = 0 ;
		try {
			PreparedStatement st = conn.prepareStatement(sql);
			st.setString(2, item.asin);
			st.setString( 1, item.distro);
			ResultSet rs = st.executeQuery();
			while( rs.next()){
				cartCount++;
				String account = rs.getString("c.account");
				String distro = rs.getString("c.distro");
				Double cartPrice = rs.getDouble("c.price");
				Double productPrice = rs.getDouble("ap.retail_cost") ;
				EtailerPrices.log("\t"+item.getId() + " has a new price of " + item.retail_cost) ;
				EtailerPrices.log("\tCart for account "+ account + " has a price of " + cartPrice.toString()) ;
			}
		}catch( SQLException sEx){
			EtailerPrices.error(sEx.getMessage());
		}
		EtailerPrices.log("\t"+"Found " + cartCount + " carts for " + item.getId());
		*/
	}

	public static void writeData(PreparedStatement st) throws SQLException {
	    if( EtailerPrices.writeAllData) {
			if (st.executeUpdate() == 0)
				EtailerPrices.error("\t\tError writing data for this entry.");
		}else
	    	EtailerPrices.log("\t"+"No actual data written to database.");
    }
    
	public static void main( String[] args )
    {
        if ( args.length < 1) {
        	EtailerPrices.fatal( "Error! Please pass file name to upload");
        	return;
        }
		EtailerPrices.log( "Starting Etailer Price request processing.");
		Connection conn = null;
		try {
			conn = getDbConnectionObject("prod");  // make this a command line option 3-29
			if( args.length >= 2 )
				EtailerPrices.whatToDo(args[1]);
			    // EtailerPrices.writeAllData = true ;
			if(!EtailerPrices.writeAllData)
			    EtailerPrices.log("Not running live, will be doing everything EXCEPT writing data to the DB");
			if( args.length == 3 && !args[2].equals("")){
				EtailerPrices.testCount = Integer.parseInt(args[2]);
			}
			parseFile(args[0], conn);
		} catch (Exception e) {
			EtailerPrices.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			EtailerPrices.log("Completed Etailer Price request processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				EtailerPrices.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }
    private static void whatToDo( final String what){
		if( what.equals("live"))
			EtailerPrices.writeAllData = true ;
		if( what.equals("verify"))
			EtailerPrices.verifyOnly = true ;
	}
}
