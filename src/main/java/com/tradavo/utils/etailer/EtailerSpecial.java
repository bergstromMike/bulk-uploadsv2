package com.tradavo.utils.etailer;

import com.opencsv.CSVReader;
import com.tradavo.utils.App;
import com.tradavo.utils.database.DBConnector;
import com.tradavo.utils.lineitems.etailer.EtailerSpecialLine;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 *
 */
public class EtailerSpecial
{
	public static Logger logger = LogManager.getLogger(EtailerSpecial.class) ;
	public static int updateCount = 0 ;
	public static int differenceCount = 0;
	public static int sameCount = 0;
	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static boolean verifyOnly ;
	private static Connection getDbConnectionObject(final String type) throws Exception {
			try {
				EtailerSpecial.log("Using " + type + " database connection.") ;
				return DBConnector.getConnection(type) ;
			} catch (SQLException ex) {
				// handle any errors
				throw new Exception ( ex);
			}
	}
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static void parseFile(String fileName, Connection conn) throws Exception {
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);
		try {
		CSVReader reader = new CSVReader(new FileReader(fileName), ',');
		EtailerSpecial.log("Processing product load file " + fileName);
		//List<Object> list = new ArrayList<>();
		//try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName),Charset.forName("Cp1252"))) {
			// br returns as stream and convert it into a List
			reader.readNext();
			int index = 1;
			String[] record = null;
			while((record = reader.readNext()) != null){
				//App.log ( "Record:"+ index);// a separator line
				try {
					if( EtailerSpecial.testCount > 0 && index > EtailerSpecial.testCount )
						break ;
					if( !verifyOnly )
						processRow(record, conn);
					else
						validateRow(record,conn);
				}
				catch ( Exception e) {
					EtailerSpecial.log ( "Error Processing Record " + index + e.getMessage());
				}
				index++ ;
			}
			
			reader.close();
			EtailerSpecial.log("Total products processed:" + --index );
			if(!verifyOnly) {
				EtailerSpecial.log("Total products updated " + updateCount);
			}else{
				EtailerSpecial.log("Price differences " + differenceCount);
				EtailerSpecial.log("Prices the same  " + sameCount);
			}
		} catch (IOException e) {
			throw new Exception("IO error when trying to read error count file " + fileName, e);
		} catch (NumberFormatException e) {
			throw new Exception("Unable to parse value in error file.", e);
		}
	}

	private static boolean productExists ( Connection conn, EtailerSpecialLine item)  throws SQLException{
		String sql = "Select 1 From product.amazon_product Where distro = ? and asin = ? and vendor_item_id = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, item.distro);
			st.setString(2, item.asin);
			st.setString(3, item.vendorItemId);
			rs = st.executeQuery();
			if ( !rs.next() ) {
				return false;
			}
			else
				return true;
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}

	private static boolean validateExists ( String sql, Connection conn, String param)  throws SQLException{
		//validate product_group
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, param);
			rs = st.executeQuery();
			if ( !rs.next() ) {
				EtailerSpecial.error ( "Invalid  " + param + " for sql " + sql);
				return false;
			}
			else
				return true;
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}


	static final String productUpdate = "Update product.amazon_product set ";
	// static final String productMinimumUpdate = "Update product.product_minimum_record set ";

	static final String getValidationData = "select retail_cost,tradavo_cost, tradavo_cogs from product.amazon_product" +
			" where asin = ? and distro = ? and vendor_item_id = ?" ;
	
	private static String conCatFieldData(String colName, int colCnt, String colVal, Boolean stringVal) {
		return (colCnt > 0 ? "," : "") + colName + "=" + ( stringVal ? "'" : "") + colVal.replaceAll("'", "''") + ( stringVal ? "'" : ""); 
	}

	private static String generateUpdateData( final String colName, final int colCnt, final String[] field,final boolean isString){
		String sql = "";
		if ( field.length > 0) {
			sql = conCatFieldData(colName, colCnt,field[0], isString);
		}
		return sql ;
	}
	private static String generateUpdateData(final String colName, final int colCnt, final String field, final boolean isString){
		String sql = "";
		if ( field.length() > 0) {
			sql = conCatFieldData(colName, colCnt,field, isString);
		}
		return sql ;
	}

	private static void updateProduct(Connection conn, EtailerSpecialLine item) throws Exception{
		String sql = productUpdate;

		int colCnt = 0;
		//sql += generateUpdateData(" item_description", colCnt, item.item_description, true);
		//colCnt ++;

		sql +=generateUpdateData(" retail_cost", colCnt, item.retail_cost, false);
		colCnt ++;

		sql +=generateUpdateData(" tradavo_cost", colCnt, item.tradavo_cost, false);
		colCnt ++;

		String offers = item.offers.trim().toLowerCase();
		if( offers.equals("0") )
			offers = "0" ;
		else if ( offers.equals("1"))
			offers = "1";
		else
			offers = "" ;
		//sql +=generateUpdateData(" offers", colCnt, offers, false);
		String avail = item.availability.trim().toLowerCase();
		if( avail.equals("0") )
			avail = "0" ;
		else if ( avail.equals("1"))
			avail = "1";
		else
			avail = "" ;
		//sql +=generateUpdateData(" availability", colCnt, avail, false);
		sql += ", timestamp=CURRENT_DATE ";
		
		sql +=  " WHERE asin = '" + item.asin + "' And distro = '" + item.distro + "'" + " And vendor_item_id = '" + item.vendorItemId + "'";
		if( EtailerSpecial.writeAllData)
			EtailerSpecial.log("SQL for update: " + sql);
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(sql);
			writeData(st); // .executeUpdate();
		}
		catch ( SQLException e) {
			throw new Exception ( e);
		}
		finally {
			updateCount++ ;
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		
	}

    private static void processRow(String[] lineCols, Connection conn)  throws Exception{
		try
		{
			EtailerSpecialLine item = new EtailerSpecialLine(lineCols);
			EtailerSpecial.log(item.getId());
			String distroSQL = "Select 1 from product.distributors d where d.distro_code = ?";

			if ( ! validateExists(distroSQL, conn, item.distro)) {
				EtailerSpecial.error ( "Invalid distro " + item.distro + " for " + item.asin);
				EtailerSpecial.error( "A valid distro is required to process this product") ;
				return;
			}

			if ( productExists(conn, item)) {
				// 3-24 this is the test for carts to see if this product is in carts
				findCartEntries( conn, item );  // find cart entries first so we get the old price of the item
				updateProduct(conn, item);
			}else {
			}

			//conn.commit();
		}
		catch ( SQLException e) {
			//conn.rollback();
			EtailerSpecial.error(e.getMessage());
			throw new Exception ( e );
		}
		
	}

	static final String shoppingCartEntries = "select c.account, c.distro,ap.item_description ,c.item, c.price, ap.retail_cost from product.cart c" +
			" join product.amazon_product ap on c.item = ap.asin and c.distro = ap.distro" +
			" where c.distro = ? and c.item = ? ;";

	public static void findCartEntries(Connection conn, EtailerSpecialLine item){
		String sql = shoppingCartEntries ;
		int cartCount = 0 ;
		try {
			PreparedStatement st = conn.prepareStatement(sql);
			st.setString(2, item.asin);
			st.setString( 1, item.distro);
			ResultSet rs = st.executeQuery();
			while( rs.next()){
				cartCount++;
				String account = rs.getString("c.account");
				String distro = rs.getString("c.distro");
				Double cartPrice = rs.getDouble("c.price");
				Double productPrice = rs.getDouble("ap.retail_cost") ;
				EtailerSpecial.log("\t"+item.getId() + " has a new price of " + item.retail_cost) ;
				EtailerSpecial.log("\tCart for account "+ account + " has a price of " + cartPrice.toString()) ;
			}
		}catch( SQLException sEx){
			App.error(sEx.getMessage());
		}
		EtailerSpecial.log("\t"+"Found " + cartCount + " carts for " + item.getId());
	}

	private static void validateRow(String[] lineCols, Connection conn)  throws Exception{
		try
		{
			EtailerSpecialLine item = new EtailerSpecialLine(lineCols);
			PreparedStatement st = conn.prepareStatement(EtailerSpecial.getValidationData);
			st.setString(1,item.getAsin());
			st.setString(2,item.getDistro());
			st.setString(3,item.vendorItemId);
			ResultSet rs = st.executeQuery();
            boolean found = false ;
			while( rs.next()){
				found = true ;
				String retail = rs.getString("retail_cost");
				String t_cost = rs.getString("tradavo_cost");
				String comp = "" ;
				comp = itemcompare(item.getId() + "\t" +"\tRetail",retail,item.retail_cost);
				if ( comp.length() > 0) {
                    EtailerSpecial.log(comp);
                }
				comp = itemcompare(item.getId() + "\t" +"\tTradavo cost",t_cost,item.tradavo_cost);
                if ( comp.length() > 0) {
                    EtailerSpecial.log(comp);
                }
			}
			if(!found){
				EtailerSpecial.warn("\t"+item.getId() + " not found in the database.");
			}
			st.close();
		}
		catch ( SQLException e) {
			EtailerSpecial.error(e.getMessage());
			throw new Exception ( e );
		}
	}

	public static String itemcompare(final String field, String dbValue, String myValue){
		String status = "DIFFERENT";
		if(dbValue == null || dbValue.length() == 0 || dbValue.equals("0") || myValue.equals("0.0"))
			dbValue = "0.00";
		if(myValue == null || myValue.length() == 0 || myValue.equals("0") || myValue.equals("0.0"))
			myValue = "0.00";
		if(Double.parseDouble(dbValue) == Double.parseDouble(myValue))
			status = "SAME";
		if(dbValue.equals(myValue))
			status = "SAME";
		if(status.equals("DIFFERENT"))
			EtailerSpecial.differenceCount ++ ;
		else
			EtailerSpecial.sameCount ++ ;
		return field + "\t"+" "+status+"->db:"+dbValue + "\tUpload:"+myValue ;
	}


	public static void writeData(PreparedStatement st) throws SQLException {
	    if( EtailerSpecial.writeAllData) {
			if (st.executeUpdate() == 0)
				EtailerSpecial.error("\t\tError writing data for this entry.");
		}else
	    	EtailerSpecial.log("\t"+"No actual data written to database.");
    }
    
	public static void main( String[] args )
    {
        if ( args.length < 1) {
        	EtailerSpecial.fatal( "Error! Please pass file name to upload");
        	return;
        }
		EtailerSpecial.log( "Starting Etailer Special request processing.");
		Connection conn = null;
		try {
			conn = getDbConnectionObject("prod");  // make this a command line option 3-29
			if( args.length >= 2 )
				EtailerSpecial.whatToDo(args[1]);
			    // App.writeAllData = true ;
			if(!EtailerSpecial.writeAllData)
			    EtailerSpecial.log("Not running live, will be doing everything EXCEPT writing data to the DB");
			if(EtailerSpecial.verifyOnly)
				EtailerSpecial.log("Not running live, only verification of user data");
			if( args.length == 3 && !args[2].equals("")){
				EtailerSpecial.testCount = Integer.parseInt(args[2]);
			}
			parseFile(args[0], conn);
		} catch (Exception e) {
			EtailerSpecial.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			EtailerSpecial.log("Completed Etailer Special request processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				EtailerSpecial.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }
    private static void whatToDo( final String what){
		if( what.equals("live"))
			EtailerSpecial.writeAllData = true ;
		if( what.equals("verify"))
			EtailerSpecial.verifyOnly = true ;
	}
}
