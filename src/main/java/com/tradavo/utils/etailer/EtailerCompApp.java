package com.tradavo.utils.etailer;

import com.opencsv.CSVReader;
import com.tradavo.utils.database.DBConnector;
import com.tradavo.utils.lineitems.etailer.EtailerCompItemLine;
import com.tradavo.utils.lineitems.ItemLine;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 *
 */
public class EtailerCompApp
{
	public static Logger logger = LogManager.getLogger(EtailerCompApp.class) ;

	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static boolean verifyOnly ;
	private static Connection getDbConnectionObject(final String type) throws Exception {
			try {
				EtailerCompApp.log("Using " + type + " database connection.") ;
				return DBConnector.getConnection(type) ;
			} catch (SQLException ex) {
				// handle any errors
				throw new Exception ( ex);
			}
	}
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static void parseFile(String fileName, HashMap<String,EtailerCompItemLine>compList,Connection conn) throws Exception {
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);
		try {
		CSVReader reader = new CSVReader(new FileReader(fileName), ',');
		EtailerCompApp.log("Processing product load file " + fileName);
		//List<Object> list = new ArrayList<>();
		//try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName),Charset.forName("Cp1252"))) {
			// br returns as stream and convert it into a List
			reader.readNext();
			int index = 1;
			String[] record = null;
			while((record = reader.readNext()) != null){
				//App.log ( "Record:"+ index);// a separator line
				try {
					if( EtailerCompApp.testCount > 0 && index > EtailerCompApp.testCount )
						break ;
					ItemLine item = new ItemLine(record);
					EtailerCompItemLine cLine = compList.get(item.asin);
					if( cLine == null ){
						EtailerCompApp.error("No entry found in the Sauter file for " + item.asin);
						cLine = new EtailerCompItemLine(item.asin,"No entry found in the Sauter file");
					}
					processRow(item, cLine, conn);
				}
				catch ( Exception e) {
					EtailerCompApp.log ( "Error Processing Record " + index + e.getMessage());
				}
				index++ ;
			}
			reader.close();
			EtailerCompApp.log("Total products processed:" + --index );
		} catch (IOException e) {
			throw new Exception("IO error when trying to read error count file " + fileName, e);
		} catch (NumberFormatException e) {
			throw new Exception("Unable to parse value in error file.", e);
		}
	}

	private static boolean validateExists ( String sql, Connection conn, String param)  throws SQLException{
		//validate asin
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, param);
			rs = st.executeQuery();
			if ( !rs.next() ) {
				EtailerCompApp.error ( "Invalid  " + param + " for sql " + sql);
				return false;
			}
			else
				return true;
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}

	static final String compInsert = "replace into product.test_price_compare ( asin, distro,item_description, " +
			"vendor_item_id,etailer_price,march14_price" +
			") values (" + 
			"?,?,?,?,?,?" +
			") ;" ;

	private static void insertProduct(Connection conn, ItemLine item, EtailerCompItemLine eLine) throws Exception{
		String sql = compInsert;
		PreparedStatement st = null;
		ResultSet rs = null;
		EtailerCompApp.log( "Inserting product record for " + eLine.getId(item.distro));
		// set up the correct values here
		try{
			st = conn.prepareStatement(compInsert) ;
			st.setString(1,eLine.getAsin());
			st.setString(2,item.getDistro());
			st.setString(3,eLine.getItem_description());
			st.setString(4,eLine.getEtin());
			st.setString(5,eLine.getNew_price());
			st.setString(6,item.tradavo_cost);  // from march 14
			writeData(st);
		} catch ( SQLException e) {
			throw new Exception ( e);
		} finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}
	
    private static void processRow(ItemLine item, EtailerCompItemLine eLine,Connection conn)  throws Exception{
		insertProduct(conn, item, eLine);
	}


	public static void writeData(PreparedStatement st) throws SQLException {
	    if( EtailerCompApp.writeAllData)
	        st.executeUpdate() ;
	    else
	    	EtailerCompApp.log("No actual data written to database.");
    }
    
	public static void main( String[] args )
    {
        if ( args.length < 2) {
        	EtailerCompApp.fatal( "Error! Please pass file names to review");
        	return;
        }
		EtailerCompApp.log( "Starting Price comparison processing.");
		Connection conn = null;
		try {
			conn = getDbConnectionObject("staging");  // make this a command line option 3-29
			if( args.length >= 3 )
				EtailerCompApp.whatToDo(args[2]);
			if(!EtailerCompApp.writeAllData)
			    EtailerCompApp.log("Not running live, will be doing everything EXCEPT writing data to the DB");
			if( args.length == 4 && !args[3].equals("")){
				EtailerCompApp.testCount = Integer.parseInt(args[3]);
			}
			HashMap <String, EtailerCompItemLine> compList = fillCompList(args[1]);
			parseFile(args[0], compList, conn);
		} catch (Exception e) {
			EtailerCompApp.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			EtailerCompApp.log("Completed bulk compare processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				EtailerCompApp.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }
    private static HashMap<String,EtailerCompItemLine> fillCompList( final String fileName) throws Exception{
		HashMap <String, EtailerCompItemLine> compList = new HashMap<>();
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);

			CSVReader reader = new CSVReader(new FileReader(fileName), ',');
			EtailerCompApp.log("Processing product comp file " + fileName);
			reader.readNext();
			String[] record = null;
			while ((record = reader.readNext()) != null) {
				EtailerCompItemLine lineItem = new EtailerCompItemLine(record);
				compList.put(lineItem.getAsin(), lineItem);
			}
			reader.close();
		return compList ;
	}

    private static void whatToDo( final String what){
		if( what.equals("live"))
			EtailerCompApp.writeAllData = true ;
	}
}
