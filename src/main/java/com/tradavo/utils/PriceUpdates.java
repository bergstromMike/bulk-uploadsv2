package com.tradavo.utils;

import com.opencsv.CSVReader;
import com.tradavo.utils.database.DBConnector;
import com.tradavo.utils.lineitems.PriceUpdateLine;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 *
 */
public class PriceUpdates
{
	public static Logger logger = LogManager.getLogger(PriceUpdates.class) ;
	public static int updateCount = 0 ;
	public static int differenceCount = 0;
	public static int sameCount = 0;
	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static boolean verifyOnly ;

	// because the current set of csv do not have the correct distro field, we have to create it here based on the name
	// of the tab in the .xml file
	public static final String THEDISTRO = "rda" ; // or rdg or lf
	// select which database this runs against here
	public static final String CONNECTION = "staging";
	// or
	//public static final String CONNECTION = "prod";

	private static Connection getDbConnectionObject(final String type) throws Exception {
			try {
				PriceUpdates.log("Using " + type + " database connection.") ;
				return DBConnector.getConnection(type) ;
			} catch (SQLException ex) {
				// handle any errors
				throw new Exception ( ex);
			}
	}
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static void parseFile(String fileName, Connection conn) throws Exception {
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);
		try {
		CSVReader reader = new CSVReader(new FileReader(fileName), '\t');
		PriceUpdates.log("Processing product load file " + fileName);

			reader.readNext();
			int index = 1;
			String[] record = null;
			while((record = reader.readNext()) != null){
				//App.log ( "Record:"+ index);// a separator line
				try {
					if( PriceUpdates.testCount > 0 && index > PriceUpdates.testCount )
						break ;
					if( !verifyOnly )
						processRow(THEDISTRO,record, conn);
				}
				catch ( Exception e) {
					PriceUpdates.log ( "Error Processing Record " + index + e.getMessage());
				}
				index++ ;
			}
			
			reader.close();
			PriceUpdates.log("Total products processed:" + --index );
			if(!verifyOnly) {
				PriceUpdates.log("Total products updated " + updateCount);
			}else{
				PriceUpdates.log("Price differences " + differenceCount);
				PriceUpdates.log("Prices the same  " + sameCount);
			}
		} catch (IOException e) {
			throw new Exception("IO error when trying to read error count file " + fileName, e);
		} catch (NumberFormatException e) {
			throw new Exception("Unable to parse value in error file.", e);
		}
	}

	private static boolean productExists ( Connection conn, PriceUpdateLine item)  throws SQLException{
		String sql = "Select 1 From product.amazon_product Where distro = ? and asin = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, item.distro);
			st.setString(2, item.asin);
			rs = st.executeQuery();
			if ( !rs.next() ) {
				return false;
			}
			else
				return true;
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}

	private static boolean validateExists ( String sql, Connection conn, String param)  throws SQLException{
		//validate product_group
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, param);
			rs = st.executeQuery();
			if ( !rs.next() ) {
				PriceUpdates.error ( "Invalid  " + param + " for sql " + sql);
				return false;
			}
			else
				return true;
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}


	static final String productUpdate = "Update product.amazon_product set ";
	// static final String productMinimumUpdate = "Update product.product_minimum_record set ";

	static final String getValidationData = "select retail_cost,tradavo_cost, tradavo_cogs from product.amazon_product" +
			" where asin = ? and distro = ? and vendor_item_id = ?" ;
	
	private static String conCatFieldData(String colName, int colCnt, String colVal, Boolean stringVal) {
		return (colCnt > 0 ? "," : "") + colName + "=" + ( stringVal ? "'" : "") + colVal.replaceAll("'", "''") + ( stringVal ? "'" : ""); 
	}

	private static String generateUpdateData( final String colName, final int colCnt, final String[] field,final boolean isString){
		String sql = "";
		if ( field.length > 0) {
			sql = conCatFieldData(colName, colCnt,field[0], isString);
		}
		return sql ;
	}
	private static String generateUpdateData(final String colName, final int colCnt, final String field, final boolean isString){
		String sql = "";
		if ( field.length() > 0) {
			sql = conCatFieldData(colName, colCnt,field, isString);
		}
		return sql ;
	}

	private static void updateProduct(Connection conn, PriceUpdateLine item) throws Exception{
		String sql = productUpdate;

		int colCnt = 0;

		sql +=generateUpdateData(" retail_cost", colCnt, item.retail_cost, false);
		colCnt ++;

		sql +=generateUpdateData(" tradavo_cost", colCnt, item.tradavo_cost, false);
		colCnt ++;

		sql +=  " WHERE asin = '" + item.asin + "' And distro = '" + item.distro + "'";
		if( PriceUpdates.writeAllData)
			PriceUpdates.log("Writing prices for : " + item.getId());
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(sql);
			writeData(st); // .executeUpdate();
		}
		catch ( SQLException e) {
			throw new Exception ( e);
		}
		finally {
			updateCount++ ;
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		
	}

    private static void processRow(final String distro, String[] lineCols, Connection conn)  throws Exception{
		try
		{
			PriceUpdateLine item = new PriceUpdateLine(distro,lineCols);
			PriceUpdates.log(item.getId());
			String distroSQL = "Select 1 from product.distributors d where d.distro_code = ?";

			if ( ! validateExists(distroSQL, conn, item.distro)) {
				PriceUpdates.error ( "Invalid distro " + item.distro + " for " + item.asin);
				PriceUpdates.error( "A valid distro is required to process this product") ;
				return;
			}

			if ( productExists(conn, item)) {
				// 3-24 this is the test for carts to see if this product is in carts
				findCartEntries( conn, item );  // find cart entries first so we get the old price of the item
				updateProduct(conn, item);
			}else {
				PriceUpdates.log("Not found:"+ item.getId());
			}

			//conn.commit();
		}
		catch ( SQLException e) {
			//conn.rollback();
			PriceUpdates.error(e.getMessage());
			throw new Exception ( e );
		}
	}

	static final String shoppingCartEntries = "select c.account, c.distro,ap.item_description ,c.item, c.price, ap.retail_cost from product.cart c" +
			" join product.amazon_product ap on c.item = ap.asin and c.distro = ap.distro" +
			" where c.distro = ? and c.item = ? ;";

	public static void findCartEntries(Connection conn, PriceUpdateLine item){
		return;
		/*String sql = shoppingCartEntries ;
		int cartCount = 0 ;
		try {
			PreparedStatement st = conn.prepareStatement(sql);
			st.setString(2, item.asin);
			st.setString( 1, item.distro);
			ResultSet rs = st.executeQuery();
			while( rs.next()){
				cartCount++;
				String account = rs.getString("c.account");
				String distro = rs.getString("c.distro");
				Double cartPrice = rs.getDouble("c.price");
				Double productPrice = rs.getDouble("ap.retail_cost") ;
				PriceUpdates.log("\t"+item.getId() + " has a new price of " + item.retail_cost) ;
				PriceUpdates.log("\tCart for account "+ account + " has a price of " + cartPrice.toString()) ;
			}
		}catch( SQLException sEx){
			PriceUpdates.error(sEx.getMessage());
		}
		PriceUpdates.log("\t"+"Found " + cartCount + " carts for " + item.getId());
		*/
	}

	public static void writeData(PreparedStatement st) throws SQLException {
	    if( PriceUpdates.writeAllData) {
			if (st.executeUpdate() == 0)
				PriceUpdates.error("\t\tError writing data for this entry.");
		}else
	    	PriceUpdates.log("\t"+"No actual data written to database.");
    }
    
	public static void main( String[] args )
    {
        if ( args.length < 1) {
        	PriceUpdates.fatal( "Error! Please pass file name to upload");
        	return;
        }
		PriceUpdates.log( "Starting Price request processing.");
		Connection conn = null;
		try {
			conn = getDbConnectionObject(CONNECTION);  // make this a command line option 3-29
			if( args.length >= 2 )
				PriceUpdates.whatToDo(args[1]);
			    // PriceUpdates.writeAllData = true ;
			if(!PriceUpdates.writeAllData)
			    PriceUpdates.log("Not running live, will be doing everything EXCEPT writing data to the DB");
			if( args.length == 3 && !args[2].equals("")){
				PriceUpdates.testCount = Integer.parseInt(args[2]);
			}
			parseFile(args[0], conn);
		} catch (Exception e) {
			PriceUpdates.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			PriceUpdates.log("Completed Price request processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				PriceUpdates.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }
    private static void whatToDo( final String what){
		if( what.equals("live"))
			PriceUpdates.writeAllData = true ;
		if( what.equals("verify"))
			PriceUpdates.verifyOnly = true ;
	}
}
