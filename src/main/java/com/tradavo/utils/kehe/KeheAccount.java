package com.tradavo.utils.kehe;

public class KeheAccount {
    private String account;
    private String accountName;
    private String zipCode ;
    private String state ;
    private String productGroup ;

    public KeheAccount(final String account, final String zipCode, final String state, final String accountName){
        this.setAccount(account);
        this.setZipCode(zipCode);
        this.setState(state);
        this.setAccountName(accountName);
    }

    public String info(){
        return account + "\t" + accountName + "\t" ;
    }
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
