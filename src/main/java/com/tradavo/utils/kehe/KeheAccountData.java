package com.tradavo.utils.kehe;

import com.tradavo.utils.KeheZipcodes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class KeheAccountData {

    private Connection conn ;

    private String accountSQL = "select a.account, a.accountname,aa.ship_zip,aa.ship_state from users.accounts a " +
            "inner join users.account_address aa on aa.account = a.account " +
            "where aa.ship_zip = ? and aa.ship_state = ? and a.active >=1 ;" ;
    private String writeProductMap = "replace into cust_product_map (account,product_group) values " +
            "(?,?);";
    private static String allAccountsSQL = "select a.account from users.accounts a "+
            "inner join users.account_address aa on aa.account = a.account " +
            "where aa.ship_zip is not null and and a.account_type = 'M' and a.active >= 1 " +
            "order by a.account;";

    public KeheAccountData(Connection conn){
        this.conn = conn ;
    }

    public static List<String> getActiveAccounts(Connection conn) throws SQLException{
        List<String> acctList = new ArrayList<>();
        String sql = allAccountsSQL ;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = conn.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                String acct = rs.getString("account");
                acctList.add(acct);
            }
        } catch (SQLException se){
            KeheZipcodes.error(se.getMessage());
            throw new SQLException( se );
        }
        finally {
            if ( st != null && !st.isClosed())
                st.close();

            if ( rs != null && !rs.isClosed())
                rs.close();
        }
        return acctList ;
    }

    public List<KeheAccount> getZipCodeAccounts(final String zipCode, final String state) throws SQLException {
        List<KeheAccount> stateList = new ArrayList<>();
        String sql = accountSQL ;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            st = this.conn.prepareStatement(sql);
            st.setString(1,zipCode);
            st.setString(2,state);
            rs = st.executeQuery();
            while(rs.next()){
                String acct = rs.getString("account");
                String name = rs.getString("accountname");
                String zip = rs.getString("ship_zip");
                String stte = rs.getString("ship_state");
                KeheAccount acc = new KeheAccount(acct,zip,stte,name);
                stateList.add(acc);
            }
        } catch (SQLException se){
            KeheZipcodes.error(se.getMessage());
            throw new SQLException( se );
        }
        finally {
                if ( st != null && !st.isClosed())
                    st.close();

                if ( rs != null && !rs.isClosed())
                    rs.close();
        }
        return stateList ;
    }
}
