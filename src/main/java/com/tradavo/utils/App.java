package com.tradavo.utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;
import com.tradavo.utils.database.DBConnector;
import com.tradavo.utils.lineitems.ItemLine;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 *
 */
public class App
{
	public static Logger logger = LogManager.getLogger(App.class) ;
	public static int updateCount = 0 ;
	public static int insertCount = 0 ;
	public static int inventoryUpdate = 0 ;
	public static int itemMasterWrites = 0 ;
	public static int differenceCount = 0;
	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static int skipInactive = 0 ;
	public static boolean verifyOnly ;
	public static boolean isKEHE = false;
	public static boolean isKeHE41 = false ;
	public static int phase = 1 ;   // FOR KEHE set this value to  specify which items get processed in a file
	public static int outOfPhase = 0 ;
	public static int notPlanogram = 0 ;
	public static List<KeheCategory> keheCategories = new ArrayList<>();
	public static List<String> unMapped = new ArrayList();

	private static Connection getDbConnectionObject(final String type) throws Exception {
			try {
				App.log("Using " + type + " database connection.") ;
				return DBConnector.getConnection(type) ;
			} catch (SQLException ex) {
				// handle any errors
				throw new Exception ( ex);
			}
	}
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static void parseFile(String fileName, Connection conn) throws Exception {
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);
		try {
			char separator = fileName.endsWith("tsv") ? '\t' : ',';
			CSVReader reader = new CSVReader(new FileReader(fileName), separator);
			App.log("Processing product load file " + fileName);
			if (fileName.contains("KeHE"))
				App.isKEHE = true;
			if (App.isKEHE) {
				App.loadKeHECategories(conn);
				if(fileName.contains("DC41"))
					App.isKeHE41 = true ;
			}
			//List<Object> list = new ArrayList<>();
			//try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName),Charset.forName("Cp1252"))) {
			// br returns as stream and convert it into a List
			if (App.isKEHE && App.keheCategories.size() == 0){
				throw new Exception("Did you forget to create the KeHE categories database?");
			}
			reader.readNext();
			int index = 1;
			String[] record = null;
			int errors = 0 ;
			while((record = reader.readNext()) != null){
				//App.log ( "Record:"+ index);// a separator line
				try {
					if( App.testCount > 0 && index > App.testCount )
						break ;
					if( !verifyOnly )
						processRow(record, conn);
					else
						validateRow(record,conn);
				}
				catch ( Exception e) {
					App.log ( "Error Processing Record " + index + " " + e.getMessage());
					errors += 1 ;
				}
				index++ ;
				App.log("\t\t\tProcessing record: " + index );
			}
			
			reader.close();
			App.log("Total products processed:" + --index );
			for(String s : unMapped){
				App.log("Unmapped KeHE category: "+ s);
			}
			if(!verifyOnly) {
				App.log("Total products added " + insertCount);
				App.log("Total products updated " + updateCount);
				App.log("Inventory updates " + inventoryUpdate);
				App.log("Item master " + (!writeAllData ? "test" : "") + " updates " + itemMasterWrites);
				App.log("Line Item errors: " + errors);
				App.log("Inactive items being added: " + App.skipInactive);
				if (App.isKEHE){
					App.log("Items skipped as Out Of Phase " + App.phase + " is " + App.outOfPhase);
					if(App.isKeHE41)
						App.log("Items skipped as not Planogram = " + App.notPlanogram);
				}
			}else
				App.log("Price differences " + differenceCount);
		} catch (IOException e) {
			throw new Exception("IO error when trying to read error count file " + fileName, e);
		} catch (NumberFormatException e) {
			throw new Exception("Unable to parse value in error file.", e);
		}
	}
	public final static String loadKeheCategoriesSQL = "select distinct(kehe_category) from product.kehe_categories ;";
	public final static String loadKeheCategorySQL = "select merchant_category from product.kehe_categories where kehe_category = ? ;";
	private static void loadKeHECategories(Connection conn) throws SQLException{
		App.log("Loading KeHE categories for merchant mapping...");
	    PreparedStatement st = null,st2 = null;
		ResultSet rs = null,rs2 = null ;
		try {
			st = conn.prepareStatement(loadKeheCategoriesSQL);
			rs = st.executeQuery();
			st2 = conn.prepareStatement(loadKeheCategorySQL);
			KeheCategory kc = null ;
			while (rs.next()) {
				kc = new KeheCategory(rs.getString(1));
				st2.setString(1,kc.getCategory());
				rs2 = st2.executeQuery();
				while(rs2.next()){
					kc.addMerchantCat(rs2.getString(1));
				}
				App.keheCategories.add(kc);
			}
			// do some log update here....
		}catch(SQLException e){

		}finally{
			if ( st != null && !st.isClosed())
				st.close();
			if ( st2 != null && !st2.isClosed())
				st2.close();
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}
	private static boolean productExists ( Connection conn, ItemLine item)  throws SQLException{
		//validate product_group
		String sql = "Select 1 From product.amazon_product Where distro = ? and asin = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, item.distro);
			st.setString(2, item.asin);
			rs = st.executeQuery();
			if ( !rs.next() ) {
				return false;
			}
			else
				return true;
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}

	private static boolean validateExists ( String sql, Connection conn, String param)  throws SQLException{
		//validate product_group
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, param);
			rs = st.executeQuery();
			if ( !rs.next() ) {
				App.error ( "Invalid  " + param + " for sql " + sql);
				return false;
			}
			else
				return true;
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}
	
	private static Long getItemMasterId ( Connection conn, Long param, ItemLine item)  throws SQLException{
		//validate product_group
		String sql = "select * from product.item_master p where p.upc_item_numeric = ?";

		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = conn.prepareStatement(sql);
			st.setLong(1, param);
			rs = st.executeQuery();
			if ( !rs.next() ) {
				return null;
			}
			else {
				item.itemMasterId= rs.getLong("id");
		        item.planDesc = rs.getString( "description_design");
		        item.prodType = rs.getString( "product_type");
		        item.meltable = rs.getInt( "meltable") == 1 ? "Y" : "N" ;
		        item.visuality = rs.getInt( "visuality_flag") == 1 ? "Y" : "N" ;;
		        //item.price_category = rs.getInt( "price_category");
				App.log("\tItem Master Id found for UPC: " + param) ;
				return item.itemMasterId;
			}
				
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		
	}
	
	static final String itemMasterUpdate = "update product.item_master set" + 
			" description_design=?, upc_item=?, upc_case=?,  " + 
			"			length_item=?, length_case=?, width_item=?,  " + 
			"			width_case=?, height_item=?, height_case=?,  " + 
			"			weight_item=?, weight_case=?, image_pog=?,  " + 
			"			image_thumbnail=?, image_large=?, retail_item_price1=?,  " + 
			"			active=?, product_type=?, meltable=?, visuality_flag=?" + 
			"			Where upc_item_numeric = ?";
			
	static final String itemMasterInsert = "INSERT INTO product.item_master" + 
			"(description_design, upc_item, upc_case, " + 
			"length_item, length_case, width_item, " + 
			"width_case, height_item, height_case, " + 
			"weight_item, weight_case, image_pog, " + 
			"image_thumbnail, image_large, retail_item_price1, " + 
			"active, product_type, meltable, visuality_flag, upc_item_numeric)" + 
			"VALUES(?,?,?," + 
			"?,?,?," + 
			"?,?,?," + 
			"?,?,?," + 
			"?,?,?," + 
			"?,?,?, ?,? )";
	
	static final String productInsert = "Insert into product.amazon_product ( asin, distro, item_description, category, " + 
			"unit_count, upc, case_upc , master_case_upc , " + 
			"gtin , man_product_no , availability , " + 
			"retail_cost, retail_suggested, tradavo_cost, tradavo_cogs, " +
			// field added 3-11 MPB
			"weight, item_length , item_height, item_width, case_width, " + 
			"case_length , case_height, manufacturer, " + 
			"small_image, large_image , pog_image," +
			"vendor_item_id, product_source ," +  // 4-5 MPB
			"meltable, new_flag , special_order, item_master_id, offers,timestamp " +
			") values (" + 
			"?,?,?,?," + 
			"?,?,?,?," + 
			"?,?,?," + 
			"?,?,?,?," +
			"?,?,?,?, ?," + 
			"?,?,?," + 
			"?,?,?," +
			"?,?," +
			"?,?,?,?,?,CURRENT_DATE) ;" ;
	static final String productUpdate = "Update product.amazon_product set ";
	// static final String productMinimumUpdate = "Update product.product_minimum_record set ";
	static final String productMinimumInsert = "replace into product.product_minimum_record(asin,minimum,maximum)" +
			"values(?,?,?)";
	
	static final String productgroupItemInsert  = "Insert INTO product.product_group_item" + 
			"(groupid, asin, distro, category, order_group, sort_order, active)" + 
			"VALUES(?, ?, ?, ?, ?, ?, 1) On Duplicate Key Update order_group = ?, sort_order=?";

	static final String productFeaturesInsert = "REPLACE INTO product.product_features  (asin, distro, keywords, features) VALUES(?, ?, ?, ?)";

	static final String productBrandHierarchy = "REPLACE INTO product.brand_hierarchy (itemid, distro, product_line, brand, division, manufacturer, product_search) " + 
			"values ( ?,?,?,?,?,?,'')";
	
	static final String updateActiveProductCount = "update product.amazon_product a set a.active_product_count =" + 
			"(select count(*) From product.product_group_item where active = 1 and asin = ? and distro = ? )" + 
			"Where asin = ? and distro = ?";
	
	static final String insertInventoryCount = "insert into product.inventory (itemid, distro, item_count) values (?,?,?) " +
						"On Duplicate Key Update item_count = ?";
	// 3-24 testing carts
	static final String shoppingCartEntries = "select c.account, c.distro,ap.item_description ,c.item, c.price, ap.retail_cost from product.cart c" +
			" join product.amazon_product ap on c.item = ap.asin and c.distro = ap.distro" +
			" where c.distro = ? and c.item = ? ;";
	// price categories
	static final String validPriceCategory = "select id from product.price_category where id = ?";
	static final String updatePriceCategory = "update product.item_master set price_category = ? where id = ?" ;

	static final String getValidationData = "select retail_cost, retail_suggested,tradavo_cost, tradavo_cogs from product.amazon_product" +
			" where asin = ? and distro = ? " ;
	static final String insertProductIndex = "replace into product.product_words (word) values (?);";

	private static void insertProductBrandHierarchy(Connection conn, ItemLine item) throws Exception{
		App.log( "\tUpdating ProductBrandHierarchy for " + item.getId());
		String sql = productBrandHierarchy;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			int colIndex = 1;
			
			st = conn.prepareStatement(sql);
			st.setString(colIndex ++, item.asin);
			st.setString(colIndex ++, item.distro);
			st.setString(colIndex ++, item.prodLine);
			st.setString(colIndex ++, item.brand);
			st.setString(colIndex ++, item.division);
			st.setString(colIndex ++, item.manufacturer);
			writeData(st); // .executeUpdate();
		}
		catch ( SQLException e ) {
			throw new Exception ( e);
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}

	private static void updateProductActiveProductCount(Connection conn, ItemLine item) throws Exception{
		String sql = updateActiveProductCount;
		App.log( "\tUpdating ActiveProductCount for " + item.getId());
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			int colIndex = 1;
			
			st = conn.prepareStatement(sql);
			st.setString(colIndex ++, item.asin);
			st.setString(colIndex ++, item.distro);
			st.setString(colIndex ++, item.asin);
			st.setString(colIndex ++, item.distro);
			writeData(st); // .executeUpdate();
		}
		catch ( SQLException e ) {
			throw new Exception ( e);
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}
	private static void insertInventoryCount(Connection conn, ItemLine item) throws Exception{
		App.log( "\tUpdating inventory count for " + item.getId());
		String sql = insertInventoryCount;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			int colIndex = 1;
			
			Long inventoryCountL = null;
			
			try {
				inventoryCountL = Long.parseLong(item.inventoryCount);
			}
			catch(NumberFormatException e ) {
				App.error ( "Unable to parse inventoryCount " + item.inventoryCount + " to long. Invalid number" + e.getMessage());
			}
			if ( inventoryCountL == null )
				return;
			
			st = conn.prepareStatement(sql);
			st.setString(colIndex ++, item.asin);
			st.setString(colIndex ++, item.distro);
			st.setLong(colIndex ++, inventoryCountL);
			st.setLong(colIndex ++, inventoryCountL); //for update
			writeData(st); // .executeUpdate();
		}
		catch ( SQLException e ) {
			throw new Exception ( e);
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
			App.inventoryUpdate++ ;
		}
	}

	private static void insertProductFeatures(Connection conn, ItemLine item) throws Exception{
		App.log( "\tUpdating ProductFeatures for " + item.getId());
		String sql = productFeaturesInsert;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			int colIndex = 1;
			
			st = conn.prepareStatement(sql);
			st.setString(colIndex ++, item.asin);
			st.setString(colIndex ++, item.distro);
			st.setString(colIndex ++, item.keyWords);
			st.setString(colIndex ++, item.addF);
			writeData(st); //.executeUpdate();
		}
		catch ( SQLException e ) {
			throw new Exception ( e);
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}
	
	private static String conCatFieldData(String colName, int colCnt, String colVal, Boolean stringVal) {
		return (colCnt > 0 ? "," : "") + colName + "=" + ( stringVal ? "'" : "") + colVal.replaceAll("'", "''") + ( stringVal ? "'" : ""); 
	}

	private static String generateUpdateData( final String colName, final int colCnt, final String[] field,final boolean isString){
		String sql = "";
		if ( field.length > 0) {
			sql = conCatFieldData(colName, colCnt,field[0], isString);
		}
		return sql ;
	}
	private static String generateUpdateData(final String colName, final int colCnt, final String field, final boolean isString){
		String sql = "";
		if ( field.length() > 0) {
			sql = conCatFieldData(colName, colCnt,field, isString);
		}
		return sql ;
	}

	private static void updateProduct(Connection conn, ItemLine item) throws Exception{
		String sql = productUpdate;
		Boolean meltable = item.meltable.trim().toUpperCase().equals("Y") ? true : false;
		Boolean newFlag = item.new_flag.trim().toUpperCase().equals("Y") ? true : false;
		Boolean specialOrder = item.special_order.trim().toUpperCase().equals("Y") ? true : false;

		int colCnt = 0;
		sql += generateUpdateData(" item_description", colCnt, item.item_description, true);
		colCnt ++;

		// test cleanup of this code
		sql += generateUpdateData(" category", colCnt, item.categories, true);
		colCnt ++;
		// test the cleanup of this code
		sql +=generateUpdateData(" unit_count", colCnt, item.unit_count, false);
		colCnt ++;
		sql +=generateUpdateData(" upc", colCnt, item.upc, true);
		colCnt ++;
		sql +=generateUpdateData(" case_upc", colCnt, item.case_upc, true);
		colCnt ++;
		sql +=generateUpdateData(" master_case_upc", colCnt, item.master_case_upc, true);
		colCnt ++;
		sql +=generateUpdateData(" gtin", colCnt, item.gtin, true);
		colCnt ++;
		sql +=generateUpdateData(" man_product_no", colCnt, item.man_product_no, true);
		colCnt ++;
		sql +=generateUpdateData(" availability", colCnt, item.availability, true);
		colCnt ++;
		sql +=generateUpdateData(" retail_cost", colCnt, item.retail_cost, false);
		colCnt ++;
		sql +=generateUpdateData(" retail_suggested", colCnt, item.retail_suggested, true);
		colCnt ++;
		sql +=generateUpdateData(" tradavo_cost", colCnt, item.tradavo_cost, false);
		colCnt ++;
		// 3-11 added MPB
		sql +=generateUpdateData(" tradavo_cogs", colCnt, item.tradavo_cogs, false);
		colCnt ++;
		sql +=generateUpdateData(" weight", colCnt, item.weight, false);
		colCnt ++;
		sql +=generateUpdateData(" item_length", colCnt, item.item_length, false);
		colCnt ++;
		sql +=generateUpdateData(" item_height", colCnt, item.item_height, false);
		colCnt ++;
		sql +=generateUpdateData(" item_width", colCnt, item.item_width, false);
		colCnt ++;
		sql +=generateUpdateData(" case_width", colCnt, item.case_width, false);
		colCnt ++;
		sql +=generateUpdateData(" case_length", colCnt, item.case_length, false);
		colCnt ++;
		sql +=generateUpdateData(" case_height", colCnt, item.case_height, false);
		colCnt ++;
		sql +=generateUpdateData(" manufacturer", colCnt, item.manufacturer, true);
		colCnt ++;
		sql +=generateUpdateData(" small_image", colCnt, item.small_image, true);
		colCnt ++;
		sql +=generateUpdateData(" large_image", colCnt, item.large_image, true);
		colCnt ++;
		sql +=generateUpdateData(" pog_image", colCnt, item.pog_image, true);
		colCnt ++;
		sql +=generateUpdateData(" meltable", colCnt, String.valueOf(meltable), false);
		sql +=generateUpdateData(" new_flag", colCnt, item.getBooleanValue(item.new_flag), false);
		sql +=generateUpdateData(" special_order", colCnt, item.getBooleanValue(item.special_order), false);
		// 4-4-2022 MPB added two new fields
		sql +=generateUpdateData(" vendor_item_id", colCnt, item.vendorItemId, true);
		sql +=generateUpdateData(" product_source", colCnt, item.sourceName, true);

		String offers = item.offers.trim().toLowerCase();
		if( offers.equals("n") )
			offers = "0" ;
		else if ( offers.equals("y"))
			offers = "1";
		else
			offers = "" ;
		sql +=generateUpdateData(" offers", colCnt, offers, false);
		sql +=generateUpdateData(" item_master_id", colCnt, String.valueOf(item.itemMasterId), false);
		sql += ", timestamp=CURRENT_DATE ";
		
		sql +=  " WHERE asin = '" + item.asin + "' And distro = '" + item.distro + "'"; 
		
		App.log("SQL for update: " + sql);
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(sql);
			writeData(st); // .executeUpdate();
		}
		catch ( SQLException e) {
			throw new Exception ( e);
		}
		finally {
			updateCount++ ;
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		
	}

	private static void updateProductMinimums(Connection conn, ItemLine item) throws Exception{
		App.log( "\tUpdating product min-max for " + item.getId());
		String sql = productMinimumInsert ;
		if( item.emptyMinMax()){
			App.warn("\tNo minimum or maximum order quantity will be saved for asin " + item.asin);
			return ;
		}

		PreparedStatement stmt = conn.prepareStatement(sql);
		try{

			stmt.setString(1, item.asin);
			stmt.setInt(2, Integer.parseInt(item.minimum_order));
			stmt.setInt(3, Integer.parseInt(item.maximum_order)) ;
			writeData(stmt); //.executeUpdate();
		}
		catch ( SQLException e) {
			throw new Exception ( e);
		}
		finally {
			if ( stmt != null && !stmt.isClosed())
				stmt.close();
		}
		App.log("Updated product_minimum_record for asin " + item.getId());
	}

	private static void insertProduct(Connection conn, ItemLine item) throws Exception{
		String sql = productInsert;
		PreparedStatement st = null;
		ResultSet rs = null;
		App.log( "\tInserting product record for " + item.getId());
		App.warn( "\tProduct will not be searchable until Product Search indexes are updated");
		Boolean meltableB = item.meltable.trim().toUpperCase().equals("Y") ? true : false;
		Boolean newB = item.new_flag.trim().toUpperCase().equals("Y") ? true : false;
		Boolean specialB = item.special_order.trim().toUpperCase().equals("Y") ? true : false;
		
		try {
			st = conn.prepareStatement(sql);
			int colIndex = 1;

			String rC = item.retail_cost;
			String tC = item.tradavo_cost;
			String tG = item.tradavo_cogs;
			String wgt = item.weight ;
			Double retailCost = null, tCost = null,tCogs = null, weight=null;
			try
			{
				retailCost = item.convertValue(rC) ;
				tCost = item.convertValue(tC);
				tCogs = item.convertValue(tG);
				weight = item.convertValue(wgt) ;
			}
			catch ( NumberFormatException e) {
				App.error ( "Value of cost field not a valid Double value for asin " + item.getId());
			}
			
			st.setString(colIndex ++, item.asin);
			st.setString(colIndex ++, item.distro);
			st.setString(colIndex ++, item.item_description);
			
			String category = item.categories.length > 0 ? item.categories[0] : "";
			//first category goes in this table.
			st.setString(colIndex ++, category);
			st.setString(colIndex ++, item.unit_count);
			st.setString(colIndex ++, item.upc);
			st.setString(colIndex ++, item.case_upc);
			st.setString(colIndex ++, item.master_case_upc);
			st.setString(colIndex ++, item.gtin);
			st.setString(colIndex ++, item.man_product_no);
			st.setString(colIndex ++, item.availability);
			st.setDouble(colIndex ++, retailCost);
			st.setString(colIndex ++, item.retail_suggested);
			st.setDouble(colIndex ++, tCost);
			st.setDouble(colIndex ++, tCogs == null ? 0.00 : tCogs);
			st.setDouble(colIndex ++, weight == null ? 0.00 : weight);
			if ( item.isFieldEmpty(item.item_length) )
				item.item_length = "0";
			st.setString(colIndex ++, item.item_length);
			if ( item.isFieldEmpty(item.item_height))
				item.item_height = "0";
			
			st.setString(colIndex ++, item.item_height);
			if ( item.isFieldEmpty(item.item_width) )
				item.item_width = "0";
			st.setString(colIndex ++, item.item_width);
			if ( item.isFieldEmpty(item.case_width) )
				item.case_width = "0";
			st.setString(colIndex ++, item.case_width);
			if ( item.isFieldEmpty(item.case_length ))
				item.case_length = "0";
			st.setString(colIndex ++, item.case_length);
			if ( item.isFieldEmpty(item.case_height))
				item.case_height = "0";
			st.setString(colIndex ++, item.case_height);
			st.setString(colIndex ++, item.manufacturer);
			st.setString(colIndex ++, item.small_image);
			st.setString(colIndex ++, item.large_image);
			st.setString(colIndex ++, item.pog_image);
			st.setString(colIndex ++, item.vendorItemId);
			st.setString(colIndex ++, item.sourceName);
			st.setBoolean(colIndex ++, meltableB);
			st.setBoolean(colIndex ++, newB);
			st.setBoolean(colIndex ++, specialB);
			st.setLong(colIndex ++, item.itemMasterId);
			if( item.offers.trim().toLowerCase().equals("") )
				App.warn("\tProduct will be inserted as OOS.  The stock status field was empty for " + item.getId());
			st.setInt(colIndex ++, (item.offers.trim().toLowerCase().equals("y") ? 1 : 0));

			writeData(st); // .executeUpdate();
			App.log("\tCreating product index for item " + item.getId());
			sql = insertProductIndex ;
			st = conn.prepareStatement(sql);
			st.setString(1, item.asin);
			writeData(st);
			insertCount++;
		}
		catch ( SQLException e) {
			throw new Exception ( e);
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}

	private static void insertUpdateItemMaster(Connection conn, ItemLine item) throws SQLException{
		String sql = null;
		String sType = "" ;
		if ( item.itemMasterId == null) {
			sql = itemMasterInsert;
			sType = "Inserting";
		}else {
			sql = itemMasterUpdate;
			sType = "Updating ";
		}
		App.log( "\t" + sType+ " ItemMaster for " + item.getId());
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		Boolean meltableB = item.meltable.trim().toUpperCase().equals("Y") ? true : false;
		Boolean visualityB = item.visuality.trim().toUpperCase().equals("Y") ? true : false;
		
		try {
			st = conn.prepareStatement(sql);
			int colIndex = 1;

			st.setString(colIndex ++, item.planDesc);
			st.setString(colIndex ++, item.upc);
			st.setString(colIndex ++, item.case_upc);
			st.setString(colIndex ++, item.item_length.isEmpty()? "0" : item.item_length);
			st.setString(colIndex ++, item.case_length.isEmpty()? "0" : item.case_length);
			st.setString(colIndex ++, item.item_width.isEmpty() ? "0" : item.item_width); //item_width
			st.setString(colIndex ++, item.case_width.isEmpty() ? "0" : item.case_width); //case_width
			st.setString(colIndex ++, item.item_height.isEmpty()?"0":item.item_height); 
			st.setString(colIndex ++, item.case_height.isEmpty()?"0":item.case_height); 
			st.setString(colIndex ++, item.weight.isEmpty()?"0":item.weight); //item_weight 
			st.setString(colIndex ++, null); //case_weight
			st.setString(colIndex ++, item.pog_image);  
			st.setString(colIndex ++, item.small_image);  
			st.setString(colIndex ++, item.large_image);
			
			//
			String rC = item.retail_cost != null && !item.retail_cost.trim().isEmpty()?item.retail_cost.trim() : "";
			
			Float retailCost = null;
			try
			{
				
				if ( rC != "") {
					if ( rC.substring(0,1).equals("$") )
						rC = rC.substring(1);
					rC.replace(",","");
					retailCost = Float.valueOf(rC);
				}
			}
			catch ( NumberFormatException e) {
				App.error ( "\tRetail cost not a valid Float value " + rC + " for " + item.getId());
				retailCost = 0.00f;
			}
			
			st.setFloat(colIndex ++, retailCost);  
			st.setInt(colIndex ++, 1);  
			st.setString(colIndex ++, item.prodType);
			st.setBoolean(colIndex ++, meltableB);  
			st.setBoolean(colIndex ++, visualityB);  
			st.setLong(colIndex ++, item.upcL);
			
			writeData(st) ; // .executeUpdate();
			
			if ( item.itemMasterId == null ) {
				if( !verifyOnly && !writeAllData)
					item.itemMasterId = 999999L ;   // if test only mode.....
				else
					item.itemMasterId  = getItemMasterId(conn,item.upcL, item);
			}

		}
		catch ( SQLException e) {
			throw new SQLException ( e);
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
			App.itemMasterWrites++;
		}
	}
	
	private static void insertUpdateProductGroupItem (Connection conn, ItemLine item) throws Exception {
		
		//need to create a productGroupItem for each category
		//if the user provides new categories that, we need to delete and insert new
		String sql = productgroupItemInsert;

		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			if (  item.categories.length > 0) {
				//delete existing
				String delSql = "Delete from product.product_group_item Where groupid = ? and asin = ? and distro = ? and category not in (" ;
				for ( int idx = 0; idx < item.categories.length; idx ++)
						delSql +=    (idx > 0 ? ",'" : "'") + item.categories[idx].trim().replaceAll("'", "''") + "'";
				
				delSql += ")";
				
				int colIndex = 1;
				st = conn.prepareStatement(delSql);
				st.setString(colIndex++, item.prdGrp);
				st.setString(colIndex++, item.asin);
				st.setString(colIndex++, item.distro);
				writeData(st); // .executeUpdate();
				if ( !st.isClosed())
					st.close();
			}
			
			for ( String cat : item.categories) {
				int colIndex = 1;
				st = conn.prepareStatement(sql);
				st.setString(colIndex++, item.prdGrp);
				st.setString(colIndex++, item.asin);
				st.setString(colIndex++, item.distro);
				st.setString(colIndex++, cat);
				st.setString(colIndex++, item.orderGrp);
				st.setInt(colIndex++, item.sort_group);
				//for update
				st.setString(colIndex++, item.orderGrp);
				st.setInt(colIndex++, item.sort_group);
				if( item.isFieldEmpty(item.orderGrp))
					App.log("Product group " + item.prdGrp + " for " + item.getId()+" has an empty order group") ;
				writeData(st) ; // .executeUpdate();
			}
		}
		catch ( SQLException e ) {
			throw new Exception ( e);
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}
	
    private static void processRow(String[] lineCols, Connection conn)  throws Exception{
		try
		{
			ItemLine item = new ItemLine(lineCols);
			App.log(item.getId());
			if(App.isKEHE){
				if(!item.isPhase(App.phase)) {
					App.log(item.getId()+"\t\t not part of phase " + App.phase) ;
					App.outOfPhase ++ ;
					return;
				}
				if(isKeHE41 && !("Y".equals(item.getVisuality()))) {
					App.log("\t\tItem not a planogram item ") ;
					App.notPlanogram ++ ;
					return;
				}
				App.log("Add leading zeros to item\t" + item.getId());
				fixupCategories(item);
				//App.log(item.getId() + " has " + item.categories.length + " categories." );
				// return;
				if( item.offers.equals("N")) {
					App.skipInactive ++ ;
					//App.testCount++ ;
					//return;
				}
			}
			String distroSQL = "Select 1 from product.distributors d where d.distro_code = ?";
			String orderGroupSQL = "select  1 from product.order_group og where og.groupid = ?";
			String productGroupSQL = "select  1 from product.product_group pg where pg.product_group =  ?";
			boolean invalidOG = false ;
			boolean invalidPG = false ;
			//|| item.categories.length == 0
			if ( item.upc == null || item.upc.trim().isEmpty() || item.asin == null || item.asin.isEmpty() || item.distro == null || item.distro.isEmpty()  ) {
				App.error ( "\tupc, asin or distro is null or empty. All these values are required to insert or update a product.");
				//return;  // TODO should this be restored
			}
			if ( item.isFieldEmpty(item.upc ) && item.isFieldEmpty(item.case_upc) )
				App.error("\tUPC and Case UPC are empty.  No product update possible");

			if ( ! validateExists(distroSQL, conn, item.distro)) {
				App.error ( "\tInvalid distro " + item.distro + " for " + item.asin);
				App.error( "\tA valid distro is required to process this product") ;
				return;
			}

			if (item.isFieldEmpty(item.orderGrp) || !validateExists(orderGroupSQL, conn, item.orderGrp)) {
				// App.error ( "Invalid order group " + item.orderGrp + " for asin " + item.asin);
				App.error( "\tNo order group " + (item.isFieldEmpty(item.orderGrp)?"supplied" : "found") + " for " +item.getId());
				invalidOG = true;
			}
			
			if ( item.isFieldEmpty(item.prdGrp) || !validateExists(productGroupSQL, conn, item.prdGrp)) {
				App.error( "\tNo product group " + (item.isFieldEmpty(item.prdGrp)?"supplied" : "found") + " for " + item.getId());
				if( item.categories.length > 0 )
					App.error( "\tCategories cannot be entered without a valid product group.");
				invalidPG = true;
				// return ;
			}
			
			Long upcL = null;
			try {
				String upcLtest = item.isFieldEmpty(item.upc) ? item.case_upc : item.upc ;
				upcL = Long.parseLong(upcLtest);
				if ( upcL == 0 ) {
					App.error( "\t" + item.asin + " has Invalid upccode. Has to be > 0");
					return;
				}
			}
			catch ( NumberFormatException e) {
				App.error( "\t" + item.asin + " has Invalid upccode. Not a numeric value. " + item.upc);
				return;
			}

			item.upcL=upcL;
			
			Long itemMasterId = getItemMasterId(conn,upcL,item);
			
			item.itemMasterId  = itemMasterId;
			conn.setAutoCommit(false);

			
			//first insert/update itemMaster
			if ( itemMasterId == null || item.updateItemMaster.trim().toLowerCase().equals("y"))
				insertUpdateItemMaster( conn,  item);
			// price category in the item master table
			// MPB 8-1-2022
			if( item.canUpdatePriceCategory()){
				// validate the category
				if(validatePriceCategory( conn, item.priceCategory)) {
					// if valid update it
					updatePriceCategory(conn,item.priceCategory, item.itemMasterId );
				}else{
					App.log("\tPrice category is " + item.priceCategory + " and will not be updated." );
				}

			}
			if ( productExists(conn, item)) {
				// 3-24 this is the test for carts to see if this product is in carts
				findCartEntries( conn, item );  // find cart entries first so we get the old price of the item
				updateProduct(conn, item);
			}else {
				insertProduct( conn,  item);
			}

			// here we update the min/max
			updateProductMinimums(conn, item);
			
			if (  !invalidPG && (item.categories != null && item.categories.length > 0 ))
				insertUpdateProductGroupItem(conn,  item);
			else
				App.warn("\tSkipping update of product groups for this item(empty product group) " + item.getId()) ;

			updateProductActiveProductCount(conn,  item);
			if ( !item.prodLine.isEmpty() || !item.division.isEmpty() || !item.brand.isEmpty())
				insertProductBrandHierarchy(conn, item);
			else
				App.warn("\tSkipping update of brand hierarchy for this item (empty brand, division or prodline) " + item.getId()) ;

			if ( !item.keyWords.isEmpty() || !item.addF.isEmpty())
				insertProductFeatures(conn, item);
			else
				App.warn("\tSkipping update of product features for this item (empty Keywords) " + item.getId()) ;

			if ( item.inventoryCount != null && !item.inventoryCount.isEmpty())
				insertInventoryCount(conn, item);
			else
				App.warn("\tSkipping update of inventory count for this item (empty inventoryCount) " + item.getId()) ;
			conn.commit();
		}
		catch ( SQLException e) {
			conn.rollback();
			App.error(e.getMessage());
			throw new Exception ( e );
		}
	}

	private static void fixupCategories(ItemLine item) {
		String newCats = "";
		//App.log("Item Description:\t"+item.getItem_description());
		//App.log("KeHE Categories :\t"+ item.getCategoriesAsString());
		for (String s : item.categories) {
			if( s == null || s.length() == 0 )
				continue;
			String comp = s.toUpperCase().replace("  "," ");
			String merCat = App.findCategory(comp);
			if(merCat == null) {
				App.unMapped.add(s);
			}else{
				newCats += merCat+";" ;
			}
		}
		// now remove duplicates
        String[] newList = newCats.split(";");
        //App.log("Merc Categories :\t"+ newCats);
		String cleanNew = "";
		int max = newList.length > 3 ? 3 : newList.length-1 ;
		for(int idx = 0 ; idx <=max ;idx++){
		    String s = newList[idx];
		    if(!cleanNew.contains(s))
		        cleanNew += s+";";
        }
		item.categories = cleanNew.split(";");
        //App.log("New  Categories :\t"+ cleanNew);
	}

	private static String findCategory(final String comp){
		for( KeheCategory k : App.keheCategories){
			if(k.isEqual(comp))
				return k.merchantCatToString();
		}
		return null ;
	}
	// so if a category is already in the list then we cannot add it....
	private static void validateRow(String[] lineCols, Connection conn)  throws Exception{
		try
		{
			ItemLine item = new ItemLine(lineCols);
			PreparedStatement st = conn.prepareStatement(App.getValidationData);
			st.setString(1,item.getAsin());
			st.setString(2,item.getDistro());
			ResultSet rs = st.executeQuery();
            boolean written = false ;
			while( rs.next()){
				String retail = rs.getString("retail_cost");
				String suggest = rs.getString("retail_suggested");
				String t_cost = rs.getString("tradavo_cost");
				String t_cogs = rs.getString("tradavo_cogs");
				String comp = "" ;
				comp = itemcompare(item.getAsin()+"\t"+item.getDistro() + "\t" +"\tRetail",retail,item.retail_cost);
				if ( comp.length() > 0) {
                    written = true ;
                    App.log(comp);
                }
				comp = itemcompare(item.getAsin()+"\t"+item.getDistro() + "\t" +"\tSuggested ",suggest,item.retail_suggested);
                if ( comp.length() > 0) {
                    written = true ;
                    App.log(comp);
                }
				comp = itemcompare(item.getAsin()+"\t"+item.getDistro() + "\t" +"\tTradavo cost",t_cost,item.tradavo_cost);
                if ( comp.length() > 0) {
                    written = true ;
                    App.log(comp);
                }
				comp = itemcompare(item.getAsin()+"\t"+item.getDistro() + "\t" +"\tTradavo Cogs",t_cogs,item.tradavo_cogs);
                if ( comp.length() > 0) {
                    written = true ;
                    App.log(comp);
                }
			}
			st.close();
		}
		catch ( SQLException e) {
			App.error(e.getMessage());
			throw new Exception ( e );
		}
	}

	public static String itemcompare(final String field, String dbValue, String myValue){
		if(dbValue == null || dbValue.length() == 0 || dbValue.equals("0"))
			dbValue = "0.00";
		if(myValue == null || myValue.length() == 0 || myValue.equals("0"))
			myValue = "0.00";
		if(Double.parseDouble(dbValue) == Double.parseDouble(myValue))
			return "";
		if(dbValue.equals(myValue))
			return "";
		App.differenceCount ++ ;
		return field + " DIFFERENT->db:"+dbValue + "\tUpload:"+myValue ;
	}
	public static boolean validatePriceCategory( Connection conn, int priceCat) throws Exception{
		String sql = validPriceCategory ;
		if( priceCat == 0 )
			return true ;  // 0 is a valid price category
		boolean retVal = false ;
		PreparedStatement st = null ;
		ResultSet rs = null ;
		try{
			st = conn.prepareStatement(sql);
			st.setInt(1, priceCat);
			rs = st.executeQuery();
			if ( rs.next() )
				retVal = true;
		}catch( SQLException sEx){
			App.error(sEx.getMessage());
		}finally {
			if ( st != null && !st.isClosed())
				st.close();

			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		if (retVal == true )
			App.log("\tFound " + priceCat + " in the price category table ") ;
		else
			App.log("\tDid not find " + priceCat + " in the price category table. No value will be written ") ;
		return retVal ;
	}
	public static boolean updatePriceCategory( Connection conn, int priceCat, Long id) throws Exception{
		String sql = updatePriceCategory ;
		boolean retVal = false ;
		PreparedStatement st = null ;
		try{
			st = conn.prepareStatement(sql);
			st.setInt(1, priceCat);
			st.setLong(2, id) ;
			writeData(st);
			retVal = true;
		}catch( SQLException sEx){
			App.error(sEx.getMessage());
		}finally {
			if ( st != null && !st.isClosed())
				st.close();
		}
		App.log("Found " + priceCat + " in the price category table ");
		return retVal ;
	}

	// 3-24 MPB does this item have carts
	public static void findCartEntries(Connection conn, ItemLine item){
		String sql = shoppingCartEntries ;
		int cartCount = 0 ;
		try {
			PreparedStatement st = conn.prepareStatement(sql);
			st.setString(2, item.asin);
			st.setString( 1, item.distro);
			ResultSet rs = st.executeQuery();
			while( rs.next()){
				cartCount++;
				String account = rs.getString("c.account");
				String distro = rs.getString("c.distro");
				Double cartPrice = rs.getDouble("c.price");
				Double productPrice = rs.getDouble("ap.retail_cost") ;
				App.log(item.getId() + " has a new price of " + item.retail_cost) ;
				App.log("Cart for account "+ account + " has a price of " + cartPrice.toString()) ;
			}
		}catch( SQLException sEx){
			App.error(sEx.getMessage());
		}
		App.log("Found " + cartCount + " carts for " + item.getId());
	}

	public static void writeData(PreparedStatement st) throws SQLException {
	    if( App.writeAllData)
	        st.executeUpdate() ;
	    //else
	    //	App.log("\tNo actual data written to database.");
    }
    
	public static void main( String[] args )
    {
        if ( args.length < 1) {
        	App.fatal( "Error! Please pass file name to upload");
        	return;
        }
		App.log( "Starting bulk upload processing.");
		Connection conn = null;
		try {
			conn = getDbConnectionObject("prod");  // make this a command line option at some point or a config file!
			if( args.length >= 2 )
				App.whatToDo(args[1]);
			    // App.writeAllData = true ;
			if(!App.writeAllData)
			    App.log("Not running live, will be doing everything EXCEPT writing data to the DB");
			if(App.verifyOnly)
				App.log("Not running live, only verification of user data");
			if( args.length == 3 && !args[2].equals("")){
				App.testCount = Integer.parseInt(args[2]);
			}
			parseFile(args[0], conn);
		} catch (Exception e) {
			App.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			App.log("Completed bulk upload processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				App.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }
    private static void whatToDo( final String what){
		if( what.equals("live"))
			App.writeAllData = true ;
		if( what.equals("verify"))
			App.verifyOnly = true ;
	}
}
