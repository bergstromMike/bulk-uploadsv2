package com.tradavo.utils;

import com.opencsv.CSVReader;
import com.tradavo.utils.database.DBConnector;
import com.tradavo.utils.lineitems.WyndhamItemLine;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 *
 */
public class WyndhamLoaderApp
{
	public static Logger logger = LogManager.getLogger(WyndhamLoaderApp.class) ;
	public static Logger dLog = LogManager.getLogger("descriptionRef");
	public static int updateCount = 0 ;
	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static int found = 0 ;
	public static int missing = 0 ;
	public static int etinWrite = 0 ;
	private static Connection getDbConnectionObject(final String type) throws Exception {
		try {
			App.log("Using " + type + " database connection.") ;
			return DBConnector.getConnection(type) ;
		} catch (SQLException ex) {
		    // handle any errors
			throw new Exception ( ex);
		}
	}
	public static void dlog(final String account,final String desc, String id){
		//etailer = etailer.replaceAll(",","[com]");
		//merchant = merchant.replaceAll(",","[com]");
		dLog.log(Level.INFO, account+"|"+desc+"|"+id);
	}
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static void parseFile(String fileName, Connection conn) throws Exception {
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);
		try {
			// set up the first field in the csv file
			WyndhamLoaderApp.dlog("account", "desc","corp id");
		CSVReader reader = new CSVReader(new FileReader(fileName), ',');
		WyndhamLoaderApp.log("Processing product load file " + fileName);
		//List<Object> list = new ArrayList<>();
		//try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName),Charset.forName("Cp1252"))) {
			// br returns as stream and convert it into a List
			reader.readNext();
			int index = 1;
			String[] record = null;
			while((record = reader.readNext()) != null){
				try {
					if( WyndhamLoaderApp.testCount > 0 && index > WyndhamLoaderApp.testCount )
						break ;
					WyndhamLoaderApp.log ( "Record:"+ index);// a separator line
					// if( index > 2089 && index < 2091)
						processRow(record, conn);
					index++ ;
				}
				catch ( Exception e) {
					WyndhamLoaderApp.log ( "Error Processing " + index + e.getMessage());
				}
			}
			reader.close();
			WyndhamLoaderApp.log("Corporate ID values written:" + etinWrite );
			WyndhamLoaderApp.log("Total products processed:" + --index );
		} catch (IOException e) {
			throw new Exception("IO error when trying to read error count file " + fileName, e);
		} catch (NumberFormatException e) {
			throw new Exception("Unable to parse value in error file.", e);
		}
	}

	static final String accountUpdate = "Update users.accounts set corporate_account_id = ? where account = ?";

	private static void writeAccount(Connection conn, final String account, final String corpId) throws SQLException{
		PreparedStatement st = null;
		try{
			String updateStmt = accountUpdate ;
			st = conn.prepareStatement(updateStmt);
			st.setString(1, corpId);
			st.setString(2, account) ;
			etinWrite += writeData(st);
			WyndhamLoaderApp.log("DB Write \t" + account + "\t" + corpId);
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
		}
	}

    private static void processRow(String[] lineCols, Connection conn)  throws Exception{
		try
		{
			WyndhamItemLine item = new WyndhamItemLine(lineCols);
			WyndhamLoaderApp.log(item.getId());
			if ( item.getAccount() == null || item.getAccount().trim().isEmpty() ){
				WyndhamLoaderApp.error ( "Account is null or empty. All these values are required to insert or update an account.");
				return;
			}
			writeAccount(conn, item.getAccount(),item.getCorp_id());
			//conn.setAutoCommit(false);
			//conn.commit();
		}
		catch ( SQLException e) {
			conn.rollback();
			WyndhamLoaderApp.error(e.getMessage());
			throw new Exception ( e );
		}
		
	}

	public static int writeData(PreparedStatement st) throws SQLException {
	    if( WyndhamLoaderApp.writeAllData)
	        return st.executeUpdate() ;
	    else
	    	WyndhamLoaderApp.log("No Account data written to database.");
	    return 0 ;
    }
    
	public static void main( String[] args )
    {
        if ( args.length < 1) {
        	WyndhamLoaderApp.fatal( "Error! Please pass file name to upload");
        	return;
        }
		WyndhamLoaderApp.log( "Starting Wyndham account corporate id Load processing.");
		Connection conn = null;
		try {
			conn = getDbConnectionObject("prod");
			if( args.length >= 2 && args[1].equals("live"))
			    WyndhamLoaderApp.writeAllData = true ;
			else
			    WyndhamLoaderApp.log("Not running live, will be doing everything EXCEPT writing data to the DB");
			if( args.length == 3 && !args[2].equals("")){
				WyndhamLoaderApp.testCount = Integer.parseInt(args[2]);
			}
			parseFile(args[0], conn);
		} catch (Exception e) {
			WyndhamLoaderApp.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			WyndhamLoaderApp.log("Completed Wyndham account corporate id load processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				WyndhamLoaderApp.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }
}
