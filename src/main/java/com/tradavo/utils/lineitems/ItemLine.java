package com.tradavo.utils.lineitems;

import com.tradavo.utils.App;

public class ItemLine {
	public String asin,  distro,  item_description;
	public String  unit_count,  upc,  case_upc,  master_case_upc ;
	public String  gtin,  man_product_no,  availability;
	public String  retail_cost,  retail_suggested,  tradavo_cost, tradavo_cogs;
	public String minimum_order, maximum_order ;  // 3--11 MPB
	public String  weight,  item_length,  item_height, item_width;
	public String  case_length,  case_height,  case_width, manufacturer;
	public String  small_image = "pmi/small/",  large_image = "pmi/large",  pog_image;
	public String  meltable,  new_flag,  special_order;
	public String keyWords,addF, visuality ;
	public String planDesc,prodType, prodLine;
	public String brand,division, prdGrp ;
	public String orderGrp,categories[] = {};
	public Integer priceCategory ;
	public String vendorItemId, sourceName;// mpb 4-4-2022
	public String updateItemMaster;
	public Long upcL;
	public Long itemMasterId;
	public String offers;
	public String inventoryCount;
	public Integer sort_group;
	public String itemPhase = "0";

	int asinColNo = 0,  distroColNo = 1,  item_descriptionColNo = 2, planDescColNo = 3; 
	int unit_countColNo = 4,  upcColNo = 5,  case_upcColNo = 6,  master_case_upcColNo = 7; 
	int  gtinColNo = 8,  man_product_noColNo = 9,  availabilityColNo = 11; 
	int  retail_costColNo = 12,  retail_suggestedColNo = 13,  tradavo_costColNo = 14, tradavo_cogsColNo = 15; // 3/11 MPB
	int minimum_orderColNo = 16, maximum_orderColNo = 17 ;  // 3-11 MPB
	int  weightColNo = 18,  item_lengthColNo = 19,  item_heightColNo = 20, item_widthColNo = 21;
	int  case_lengthColNo = 22,  case_heightColNo = 23,  case_widthColNo = 24, manufacturerColNo = 25;
	int  small_imageColNo = 26,  large_imageColNo = 27,  pog_imageColNo = 28;
	int  meltableColNo = 29, keyWordsColNo = 30,  addFColNo = 31, new_flagColNo = 32,  special_orderColNo = 33;
	int visualityColNo = 34;
	int prodTypeColNo = 35, prodLineColNo = 36;
	int brandColNo = 37,divisionColNo = 38, prdGrpColNo = 39;
	int orderGrpColNo = 40,categoriesColNo = 41, priceCatColNo=42,vendorItemIdCol=43,sourceNameCol=44,updateItemMasterColNo = 45, offersColNo = 46;
	int inventoryCountColNo = 47;
	int sortGroupColNo = 48;
	int phaseColNo = 49 ;
	
	
	public ItemLine(String[] lineCols) {
	 asin=lineCols[asinColNo];  distro=lineCols[distroColNo].trim();  item_description = lineCols[item_descriptionColNo]; 
	  unit_count=lineCols[unit_countColNo];  upc=lineCols[upcColNo];  case_upc =lineCols[case_upcColNo];  master_case_upc  = lineCols[master_case_upcColNo]; 
	  gtin =lineCols[gtinColNo];  man_product_no =lineCols[man_product_noColNo];  availability  = lineCols[availabilityColNo]; 
	  retail_cost=lineCols[retail_costColNo];
	  retail_cost=retail_cost.replaceAll("\\$", "");
	  retail_cost=retail_cost.replaceAll(",", "");
	  retail_suggested=lineCols[retail_suggestedColNo];  
	  tradavo_cost = lineCols[tradavo_costColNo];
	  tradavo_cost = tradavo_cost.replaceAll("\\$", "");
	  tradavo_cogs = lineCols[tradavo_cogsColNo];   // 3-11 MPB this is a new field
	  tradavo_cogs = tradavo_cogs.replaceAll("\\$", "");
	  minimum_order = lineCols[minimum_orderColNo] ;  // these are new fields as well
	  maximum_order = lineCols[maximum_orderColNo] ;
	  weight=lineCols[weightColNo];
	  item_length =lineCols[item_lengthColNo]; item_length = item_length == null ? "" : item_length;   
	  item_height = lineCols[item_heightColNo];item_height = item_height == null ? "" : item_height; 
	  item_width = lineCols[item_widthColNo]; item_width = item_width == null ? "" : item_width;
	  case_length =lineCols[case_lengthColNo];case_length = case_length == null ? "" : case_length;
	  case_height=lineCols[case_heightColNo];case_height = case_height == null ? "" : case_height;
	  case_width=lineCols[case_widthColNo]; case_width = case_width == null ? "" : case_width;
	  manufacturer = lineCols[manufacturerColNo]; 
	  small_image=lineCols[small_imageColNo];  large_image =lineCols[large_imageColNo];  pog_image = lineCols[pog_imageColNo]; 
	  meltable=lineCols[meltableColNo];  new_flag =lineCols[new_flagColNo];  special_order = lineCols[special_orderColNo];	
	 keyWords =lineCols[keyWordsColNo];addF =lineCols[addFColNo]; visuality =lineCols[visualityColNo] ;
	 planDesc =lineCols[planDescColNo];prodType =lineCols[prodTypeColNo]; prodLine =lineCols[prodLineColNo] ;
	 brand =lineCols[brandColNo];division =lineCols[divisionColNo]; prdGrp =lineCols[prdGrpColNo] ;
	 orderGrp =lineCols[orderGrpColNo]; vendorItemId=lineCols[vendorItemIdCol];sourceName=lineCols[sourceNameCol];
	 if( App.isKEHE) {
		 asin = this.fixupAsin();
		 vendorItemId = asin;  // they have to be the same
	 }
	 // = lineCols[phaseColNo];
	 //else*/
	if(App.isKEHE)
	 	itemPhase = lineCols[phaseColNo];
	 // new entries on 4-4-2022 MPB
	 // and a check for valid new_flag
	 if( isFieldEmpty(this.new_flag)){
	 	new_flag = "N" ;
	 }
	 if("1".equals(new_flag)) {
		 new_flag = "Y";
		 App.error("\tNew Flag was marked as invalid.");
	 }else
	 	new_flag = new_flag;
	 //in our code no code is "default" order_group which is an empty string.
	 if ( orderGrp != null && orderGrp.trim().length() > 0 && orderGrp.toLowerCase().equals("no code"))
		 orderGrp  = "";
	 
	 String cat = lineCols[categoriesColNo].isEmpty() ? "" : lineCols[categoriesColNo];
	 
	 if ( cat.length() > 0 )
		 categories = cat.split(",");  // TODO change this to : for KEHE make it conditional based on the APP IsKehe flag
	 
	 inventoryCount = lineCols[inventoryCountColNo];
	 String pCat = lineCols[priceCatColNo] ;
	 priceCategory = pCat == null || pCat.trim().length() == 0 ? -1 : Integer.parseInt(lineCols[priceCatColNo]) ;

	 String sSort = lineCols[sortGroupColNo];
	 if ( sSort == null || sSort.trim().length() == 0) {
		 this.sort_group = 9999; //this is the default value in the DB
	 }
	 try {
			 this.sort_group = Integer.parseInt(sSort.trim());
		 }
		 catch ( NumberFormatException e  ) {
			 App.warn( "\tError converting sort_group to Integer. sGroup " + sSort + ". Default 9999 used." );
		 }

	 
	 updateItemMaster=lineCols[updateItemMasterColNo];
	 if ( updateItemMaster == null || updateItemMaster.trim().length() == 0) {
		 updateItemMaster = "Y" ; //this.new_flag.equals("Y") ? "Y":"N";
		 App.error("\tFor new items updateItemMaster must be Y.");
	 }
		 
	 offers=lineCols[offersColNo];

	 small_image = small_image.isEmpty() ? "": "pmi/small/" + small_image;
	 large_image = large_image.isEmpty() ? "" : "pmi/large/" + large_image;
	 pog_image = pog_image.trim().isEmpty() ? "" : "pog/" + pog_image ;
	}

	// another kehe specific functionality
	public String fixupAsin(){
		String result = this.asin;
		while( result.length() < 8){
			result = "0"+result ;
		}
		return result ;
	}
	public boolean isPhase(final int appPhase){
		if( appPhase == 0)
			return true ;
		final int phase = Integer.valueOf(this.itemPhase) ;
		if(phase==0)
			return false;
		return phase >= appPhase ;
	}

	public boolean canUpdatePriceCategory(){
		return priceCategory > -1 ; // if priceCategory is entered update it!  MPB 8-1-2022
	}
	public boolean emptyMinMax(){
		return (this.isFieldEmpty(minimum_order) && this.isFieldEmpty(maximum_order)) ;
	}
	public String getId(){ return ("asin/distro " + this.asin + "/"+this.distro); }

	public String getBooleanValue( final String boolStr){
		if( this.isFieldEmpty(boolStr.trim()))
			return "";
		return String.valueOf(boolStr.trim().toLowerCase().equals("y") ? true : false);
	}
	public Double convertValue( String dbStr ) throws NumberFormatException{
		if ( !dbStr.equals("")) {
			if ( dbStr.substring(0,1).equals("$") )
				dbStr = dbStr.substring(1);
			dbStr.replace(",","");
			return( Double.valueOf(dbStr));
		}
		return null ;
	}
	public boolean isFieldEmpty(final String field){
		return( field == null || field.trim().length()==0);
	}
	public String getAsin() {
		return asin;
	}

	public void setAsin(String asin) {
		this.asin = asin;
	}

	public String getDistro() {
		return distro;
	}

	public void setDistro(String distro) {
		this.distro = distro;
	}

	public String getItem_description() {
		return item_description;
	}

	public void setItem_description(String item_description) {
		this.item_description = item_description;
	}

	public String getUnit_count() {
		return unit_count;
	}

	public void setUnit_count(String unit_count) {
		this.unit_count = unit_count;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getCase_upc() {
		return case_upc;
	}

	public void setCase_upc(String case_upc) {
		this.case_upc = case_upc;
	}

	public String getMaster_case_upc() {
		return master_case_upc;
	}

	public void setMaster_case_upc(String master_case_upc) {
		this.master_case_upc = master_case_upc;
	}

	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	public String getMan_product_no() {
		return man_product_no;
	}

	public void setMan_product_no(String man_product_no) {
		this.man_product_no = man_product_no;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getRetail_cost() {
		return retail_cost;
	}

	public void setRetail_cost(String retail_cost) {
		this.retail_cost = retail_cost;
	}

	public String getRetail_suggested() {
		return retail_suggested;
	}

	public void setRetail_suggested(String retail_suggested) {
		this.retail_suggested = retail_suggested;
	}

	public String getTradavo_cost() {
		return tradavo_cost;
	}

	public void setTradavo_cost(String tradavo_cost) {
		this.tradavo_cost = tradavo_cost;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getItem_length() {
		return item_length;
	}

	public void setItem_length(String item_length) {
		this.item_length = item_length;
	}

	public String getItem_height() {
		return item_height;
	}

	public void setItem_height(String item_height) {
		this.item_height = item_height;
	}

	public String getItem_width() {
		return item_width;
	}

	public void setItem_width(String item_width) {
		this.item_width = item_width;
	}

	public String getCase_width() {
		return case_width;
	}

	public void setCase_width(String case_width) {
		this.case_width = case_width;
	}

	public String getCase_length() {
		return case_length;
	}

	public void setCase_length(String case_length) {
		this.case_length = case_length;
	}

	public String getCase_height() {
		return case_height;
	}

	public void setCase_height(String case_height) {
		this.case_height = case_height;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getSmall_image() {
		return small_image;
	}

	public void setSmall_image(String small_image) {
		this.small_image = small_image;
	}

	public String getLarge_image() {
		return large_image;
	}

	public void setLarge_image(String large_image) {
		this.large_image = large_image;
	}

	public String getPog_image() {
		return pog_image;
	}

	public void setPog_image(String pog_image) {
		this.pog_image = pog_image;
	}

	public String getMeltable() {
		return meltable;
	}

	public void setMeltable(String meltable) {
		this.meltable = meltable;
	}

	public String getNew_flag() {
		return new_flag;
	}

	public void setNew_flag(String new_flag) {
		this.new_flag = new_flag;
	}

	public String getSpecial_order() {
		return special_order;
	}

	public void setSpecial_order(String special_order) {
		this.special_order = special_order;
	}

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public String getAddF() {
		return addF;
	}

	public void setAddF(String addF) {
		this.addF = addF;
	}

	public String getVisuality() {
		return visuality;
	}

	public void setVisuality(String visuality) {
		this.visuality = visuality;
	}

	public String getPlanDesc() {
		return planDesc;
	}

	public void setPlanDesc(String planDesc) {
		this.planDesc = planDesc;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getProdLine() {
		return prodLine;
	}

	public void setProdLine(String prodLine) {
		this.prodLine = prodLine;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getPrdGrp() {
		return prdGrp;
	}

	public void setPrdGrp(String prdGrp) {
		this.prdGrp = prdGrp;
	}

	public String getOrderGrp() {
		return orderGrp;
	}

	public void setOrderGrp(String orderGrp) {
		this.orderGrp = orderGrp;
	}

	public String[] getCategories() {
		return categories;
	}
	public String getCategoriesAsString(){
		String res = "";
		for( String s :categories)
			res += s+",";
		return res ;
	}

	public void setCategories(String[] categories) {
		this.categories = categories;
	}

	public Long getUpcL() {
		return upcL;
	}

	public void setUpcL(Long upcL) {
		this.upcL = upcL;
	}

	public Long getItemMasterId() {
		return itemMasterId;
	}

	public void setItemMasterId(Long itemMasterId) {
		this.itemMasterId = itemMasterId;
	}
	
	
}