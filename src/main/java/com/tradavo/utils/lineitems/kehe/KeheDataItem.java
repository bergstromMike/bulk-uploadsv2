package com.tradavo.utils.lineitems.kehe;

public class KeheDataItem {

    private String itemid ;
    private String distro ;
    private boolean bInMerchant = false ;

    public KeheDataItem(final String itemid, final String distro){
        this.setItemid(itemid);
        this.setDistro(distro);
    }

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public String getDistro() {
        return distro;
    }

    public void setDistro(String distro) {
        this.distro = distro;
    }

    public boolean isInMerchant() {
        return bInMerchant;
    }

    public void setInMerchant(boolean bInMerchant) {
        this.bInMerchant = bInMerchant;
    }
    public String getId(){
        return itemid+"-"+distro ;
    }
}
