package com.tradavo.utils.lineitems.kehe;



public class KeheContainer {
    KeheDataItem bulk ;
    KeheDataItem weekly ;

    public KeheContainer(KeheDataItem bulk){
        this.bulk = bulk ;
    }

    public KeheContainer(KeheDataItem week, final boolean bWeekly){
        this.weekly = week ;
    }

    public String toString(){
        String ret = "";
        if(this.hasBoth() == 1)
          ret = "\tMatch: B:"+bulk.getId() + "\tW:" + weekly.getId() + "\t In merchant:" + (bulk.isInMerchant()? "yes" : "no");
        if(this.weeklyOnly() == 1)
            ret = "\tWeekly:" + weekly.getId();
        if(this.bulkOnly() == 1)
            ret = "\tBulk:" + bulk.getId()+ "\t In merchant:" + (bulk.isInMerchant()? "yes*******" : "no");
        return ret ;
    }

    public int inMerchant(){
        if( bulk != null )
            return bulk.isInMerchant() ? 1 : 0 ;
        return 0 ;
    }
    public void addWeekly(KeheDataItem week){
        this.weekly = week ;
    }
    public int hasBoth(){
        return (bulk != null && weekly != null ? 1:0);
    }
    public int weeklyOnly(){
        return bulk == null ? 1 : 0 ;
    }
    public int bulkOnly(){
        return weekly == null ? 1 : 0 ;
    }
}
