package com.tradavo.utils.lineitems.kehe;

import java.util.ArrayList;
import java.util.List;

public class KeheLineItem {
	public final static String westAmbient = "khca";
	public final static String westPerish = "khcp";
	public final static String eastAmbient = "khea";
	public final static String eastPerish = "khep";
	public final static String eastDistro = "khe";
	public final static String westDistro = "khc";
	public String zipcode,  state ;
	public String  amper,amOnly;
	public boolean bAmbient= false, bPerishable = false;
	public boolean bWest = false;
	public boolean bWriteData = true;

	int zipcodeColNo = 0,  stateColNo = 1,  amPerColNo = 3, amOnlyColNo = 4;

	public KeheLineItem(String[] lineCols) {
	 	zipcode=lineCols[zipcodeColNo];  state=lineCols[stateColNo].trim();
	 	amper = lineCols[amPerColNo];
	 	amOnly = lineCols[amOnlyColNo];
	 	if(amper.equals("Yes")){
	 		bAmbient = true ;
	 		bPerishable = true ;
	 	}
		 if(amOnly.equals("Yes")){
	 		bPerishable = false ;
	 		bAmbient = true ;
		 }
		 if( zipcode.length()==4)
		 	zipcode = "0"+zipcode ;
	}

	public void writeData(KeheLineItem other){
		if( this.bPerishable )
			other.bWriteData = false ;
		else
			this.bWriteData = false ;

	}
	public String getId(){ return (this.zipcode + "/"+this.state + "\tdistro=" +this.getDistro() + "\tambient="+bAmbient+"\tperishable="+bPerishable+" write="+bWriteData); }
	public String getDetails(){ return ("Merchant distro=" +this.getDistro() + "\tambient="+bAmbient+"\tperishable="+bPerishable); }

	public List<String> getProductGroups(){ // is it west or east we are working with
		List<String> list = new ArrayList();
		if(bPerishable)
			list.add(this.bWest ? westPerish : eastPerish);
		if(bAmbient)
			list.add(this.bWest ? westAmbient : eastAmbient);
		return list ;
	}

	public String getDistro(){
		return this.bWest ? westDistro : eastDistro ;
	}
}