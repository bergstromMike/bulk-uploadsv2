package com.tradavo.utils.lineitems;

public class PriceUpdateLine {
	public String asin,  distro,  item_description;
	public String  retail_cost,tradavo_cost;

	int asinColNo = 0,  item_descriptionColNo = 2, tradavo_costColNo = 5;
	int retail_costColNo = 6;

    // distro is passed in here because the CSV file does not contain the proper column names
	public PriceUpdateLine(final String distro, String[] lineCols) {
	 asin=lineCols[asinColNo];  this.distro=distro;  item_description = lineCols[item_descriptionColNo];

	  retail_cost=lineCols[retail_costColNo];
	  retail_cost=retail_cost.replaceAll("\\$", "");
	  asin = asin.replaceAll("\\'", "");
	  tradavo_cost = lineCols[tradavo_costColNo];
	  tradavo_cost = tradavo_cost.replaceAll("\\$", "");
	}

	public String getId(){ return ("itemid/distro " + this.asin + "/"+this.distro); }

	public boolean isFieldEmpty(final String field){
		return( field == null || field.trim().length()==0);
	}
	public String getAsin() {
		return asin;
	}

	public void setAsin(String asin) {
		this.asin = asin;
	}

	public String getDistro() {
		return distro;
	}

	public void setDistro(String distro) {
		this.distro = distro;
	}

	public String getItem_description() {
		return item_description;
	}

	public void setItem_description(String item_description) {
		this.item_description = item_description;
	}

	public String getRetail_cost() {
		return retail_cost;
	}

	public void setRetail_cost(String retail_cost) {
		this.retail_cost = retail_cost;
	}

	public String getTradavo_cost() {
		return tradavo_cost;
	}

	public void setTradavo_cost(String tradavo_cost) {
		this.tradavo_cost = tradavo_cost;
	}

}