package com.tradavo.utils.lineitems;

public class WyndhamItemLine {
	private String account;
	private String description;
	private String corp_id ;
	int accountColNo = 0,  corpIdColNo = 2, descriptionColNo = 1 ;


	public WyndhamItemLine(String[] lineCols) {
		setAccount(lineCols[accountColNo]);
		setDescription(lineCols[descriptionColNo]);
		setCorp_id( lineCols[corpIdColNo]);
		if (getCorp_id() != null && getCorp_id().trim().length() == 0)
			setCorp_id("");
	}
	public String getId(){ return ("asin/distro " + this.getAccount() + "/"+ this.getDescription()); }

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCorp_id() {
		return corp_id;
	}

	public void setCorp_id(String corp_id) {
		this.corp_id = corp_id;
	}
}