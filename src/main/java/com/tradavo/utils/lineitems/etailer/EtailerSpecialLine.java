package com.tradavo.utils.lineitems.etailer;

public class EtailerSpecialLine {
	public String asin,  distro,  item_description;
	public String  retail_cost,tradavo_cost;
	public String vendorItemId, availability;
	public String offers;

	int asinColNo = 0,  distroColNo = 1,  item_descriptionColNo = 2, tradavo_costColNo = 3;
	int retail_costColNo = 4, offersColNo = 5,  availabilityColNo = 6,  vendorItemIdCol = 7;


	public EtailerSpecialLine(String[] lineCols) {
	 asin=lineCols[asinColNo];  distro=lineCols[distroColNo].trim();  item_description = lineCols[item_descriptionColNo]; 
	  availability  = lineCols[availabilityColNo];
	  retail_cost=lineCols[retail_costColNo];
	  retail_cost=retail_cost.replaceAll("\\$", "");
	  asin = asin.replaceAll("\\'", "");
	  tradavo_cost = lineCols[tradavo_costColNo];
	  tradavo_cost = tradavo_cost.replaceAll("\\$", "");
	  vendorItemId=lineCols[vendorItemIdCol];offers=lineCols[offersColNo];
	  if( vendorItemId.equals("n/a"))
	  	vendorItemId = "Not Available";
	  //vendorItemId = vendorItemId.replaceAll("\\", "");
	}

	public String getId(){ return ("itemid/distro/etin " + this.asin + "/"+this.distro+ "/"+this.vendorItemId); }

	public boolean isFieldEmpty(final String field){
		return( field == null || field.trim().length()==0);
	}
	public String getAsin() {
		return asin;
	}

	public void setAsin(String asin) {
		this.asin = asin;
	}

	public String getDistro() {
		return distro;
	}

	public void setDistro(String distro) {
		this.distro = distro;
	}

	public String getItem_description() {
		return item_description;
	}

	public void setItem_description(String item_description) {
		this.item_description = item_description;
	}


	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getRetail_cost() {
		return retail_cost;
	}

	public void setRetail_cost(String retail_cost) {
		this.retail_cost = retail_cost;
	}

	public String getTradavo_cost() {
		return tradavo_cost;
	}

	public void setTradavo_cost(String tradavo_cost) {
		this.tradavo_cost = tradavo_cost;
	}

}