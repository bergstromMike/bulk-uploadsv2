package com.tradavo.utils.lineitems.etailer;

public class EtailerItemLine {
	public String etin, item_description;
	public String warehouseWI, warehousePA, warehouseNV ; // hold the merchant asin for a given etin
	
	int etinColNo = 0,  item_descriptionColNo = 1;
	int wiColNo = 2,  paColNo = 3,  nvColNo = 4;
	public int asinCount = 0 ;
	private int offset = 0 ;
	public EtailerItemLine(String[] lineCols) {
		if( lineCols.length > 5){
			// we have the infamous "latest version"
			offset++ ;
		}
	 	etin=lineCols[etinColNo+offset];  item_description=lineCols[item_descriptionColNo+offset].trim();
	 	warehouseWI=lineCols[wiColNo+offset];warehousePA=lineCols[paColNo+offset];warehouseNV=lineCols[nvColNo+offset];
	 	this.cleanupWarehouses();
	}

	public String getValues(){
		return(this.etin+","+this.warehouseWI+","+this.warehousePA+","+this.warehouseNV);
	}

	public String getId(){ return ("etin/description " + this.etin + "/"+this.item_description); }
	public String getItemId(){ 
		return ("\tItemId WI: " + this.warehouseWI + "\tPA:"+this.warehousePA+"\tNV:"+this.warehouseNV); }
	public String getAsinCount(){
		return ("\t has " + this.asinCount + " merchant items"); }
	
	public boolean isFieldEmpty(final String field){
		return( field == null || field.trim().length()==0);
	}
	public String getPrefix(){
		return( etin.substring(0, etin.indexOf("-")));
	}
	public int asinLength(final String asin){return(asin.length());}

	public String asinWithPrefix(final String asin){
		if( asinLength(asin) <= 11 )
			return "0" + asin ;
		return asin ;
	}

	// let's ensure that warehouses all get an asin value because
	// we have asins for this item that need etin values
	private void cleanupWarehouses() {
		String wHouse = (!this.isFieldEmpty(warehouseNV) ? warehouseNV :
				(!this.isFieldEmpty(warehousePA) ? warehousePA : warehouseWI));

		if (this.isFieldEmpty(warehousePA))
			warehousePA = wHouse;
		if (this.isFieldEmpty(warehouseNV))
			warehouseNV = wHouse;
		if(this.isFieldEmpty(warehouseWI))
			warehouseWI = wHouse ;
	}
}