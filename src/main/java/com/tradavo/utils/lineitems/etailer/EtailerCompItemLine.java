package com.tradavo.utils.lineitems.etailer;

import java.sql.Connection;

public class EtailerCompItemLine {
	public String asin;
	private String wi_asin;
	private String pa_asin;
	private String nv_asin;
	private String etin;
	private String item_description;
	private String new_price;

	int asinColNo = 0,  wiAsinColNo = 1,  paAsinColNo = 2, nvAsinColNo = 3;
	int etinColNo = 4, item_descriptionColNo = 5, newPriceColNo = 6;

	private int activeProduct = 1 ;

	public EtailerCompItemLine(String[] lineCols) {
	 asin=lineCols[asinColNo];
	  new_price = lineCols[newPriceColNo];
	  new_price = getNew_price().replaceAll("\\$", "");
	  wi_asin = lineCols[wiAsinColNo];
	  pa_asin = lineCols[paAsinColNo] ;
	  nv_asin = lineCols[nvAsinColNo] ;
	  etin = lineCols[etinColNo] ;
	  item_description=lineCols[item_descriptionColNo];
	}
	public EtailerCompItemLine(final String asin, final String desc){
		this.new_price = "" ;
		this.etin = "unknown";
		this.asin = asin;
		this.item_description = desc ;
	}

	public String getId(String distro){ return ("asin|distro|etin " + this.asin + "|"+distro+ "|"+this.etin); }

	public boolean isFieldEmpty(final String field){
		return( field == null || field.trim().length()==0);
	}
	public String getAsin() {
		return asin;
	}

	public String getItem_description() {
		return item_description;
	}

	public String getWi_asin() {
		return wi_asin;
	}

	public String getPa_asin() {
		return pa_asin;
	}

	public String getNv_asin() {
		return nv_asin;
	}

	public String getEtin() {
		return etin;
	}

	public String getNew_price() {
		return new_price;
	}

	public void writeMerchantRecord(Connection conn, final String tradavoCost){
		// let's write a datarecord for this particular item

	}

	public int getActive() {
		return activeProduct;
	}

	public void setActive(int activeProduct) {
		this.activeProduct = activeProduct;
	}
}