package com.tradavo.utils;

import com.opencsv.CSVReader;
import com.tradavo.utils.database.DBConnector;
import com.tradavo.utils.lineitems.ItemLine;
import com.tradavo.utils.lineitems.kehe.KeheContainer;
import com.tradavo.utils.lineitems.kehe.KeheDataItem;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 *
 */
public class KeheDataCompare
{
	public static Logger logger = LogManager.getLogger(KeheDataCompare.class) ;
	public static int updateCount = 0 ;
	public static int insertCount = 0 ;
	public static int inventoryUpdate = 0 ;
	public static int itemMasterWrites = 0 ;
	public static int differenceCount = 0;
	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static int skipInactive = 0 ;
	public static boolean verifyOnly ;
	public static boolean isKEHE = false;
	public static boolean isKeHE41 = false ;
	public static int phase = 1 ;   // FOR KEHE set this value to  specify which items get processed in a file
	public static int outOfPhase = 0 ;
	public static int notPlanogram = 0 ;
	public static String keheDataPath = "";
	public static String bulkPath = keheDataPath+"/bulk";
	public static String weeklyPath = keheDataPath+"/weekly";
	public static HashMap<String,KeheContainer> keheDataList = new HashMap<>();

	private static Connection getDbConnectionObject(final String type) throws Exception {
			try {
				KeheDataCompare.log("Using " + type + " database connection.") ;
				return DBConnector.getConnection(type) ;
			} catch (SQLException ex) {
				// handle any errors
				throw new Exception ( ex);
			}
	}
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static void buildBulkList(Connection conn, final String bulkPath) throws Exception{
		List<File> fileList = getListOfFiles(bulkPath);
		if(fileList == null || fileList.size() == 0)
			throw new Exception("No bulk upload files found");
		try {
			int index = 1;
			for( File f:fileList) {
				String fileName = f.getAbsolutePath();

				CSVReader reader = new CSVReader(new FileReader(fileName), '\t');
				KeheDataCompare.log("Processing Bulk load file " + fileName);
				reader.readNext();

				String[] record = null;
				int errors = 0;
				while ((record = reader.readNext()) != null) {
					KeheDataItem d = new KeheDataItem(fixupAsin(record[0]),record[1]);
					KeheContainer kc = new KeheContainer(d);
					d.setInMerchant(productExists(conn,d));
					//KeheDataCompare.log("Product merchant check " + index);
					keheDataList.put(d.getId(),kc);
					try {
						if (KeheDataCompare.testCount > 0 && index > KeheDataCompare.testCount)
							break;
						//processRow(record, conn);
					} catch (Exception e) {
						KeheDataCompare.log("Error Processing Record " + index + " " + e.getMessage());
						errors += 1;
					}
					index++;
					//KeheDataCompare.log("\t\t\tProcessing bulk upload record: " + index);
				}  // this reads the bulk upload file
				reader.close();
				index = 1 ;
			}
			KeheDataCompare.log("Total Bulk Uploader products processed:" + --index );
			index = 1 ;
			fileList = getListOfFiles(weeklyPath);
			if(fileList == null || fileList.size() == 0)
				throw new Exception("No weekly files found");
			try {
				for (File f : fileList) {
					String fileName = f.getAbsolutePath();
					CSVReader reader = new CSVReader(new FileReader(fileName), ',');
					KeheDataCompare.log("Processing weekly file " + fileName);
					reader.readNext();
					String keheDistro = fileName.contains("41TRA") ? "khc" : "khe";
					String[] record = null;
					int errors = 0;
					while ((record = reader.readNext()) != null) {
						// need to figure out the distro!
						KeheDataItem d = new KeheDataItem(fixupAsin(record[2]),keheDistro);
						String id = d.getId();
						KeheContainer kc = keheDataList.get(id);
						if( kc != null)
							kc.addWeekly(d); // add the weekly
						else {
							kc = new KeheContainer(d, true);
							keheDataList.put(d.getId(), kc);
						}
						try {
							if (KeheDataCompare.testCount > 0 && index > KeheDataCompare.testCount)
								break;
							//processRow(record, conn);
						} catch (Exception e) {
							KeheDataCompare.log("Error Processing Record " + index + " " + e.getMessage());
							errors += 1;
						}
						index++;
						//KeheDataCompare.log("\t\t\tProcessing bulk upload record: " + index);
					}  // this reads the bulk upload file
					reader.close();
				}
			}catch(Exception e){
				throw new Exception(e);
			}
			// now we have the entire block....all the bulk upload items and all the weekly items stored in one big map
			int bulkOnly = 0 ;
			int weeklyOnly = 0 ;
			int totalCt = keheDataList.values().size();
			int inMerchant = 0 ;
			int hasBoth = 0 ;
			int bulkOnlyInMerchant = 0;
			for(KeheContainer kc : keheDataList.values()){
				bulkOnly += kc.bulkOnly();
				weeklyOnly += kc.weeklyOnly();
				hasBoth += kc.hasBoth();
				inMerchant += kc.inMerchant() ;
				bulkOnlyInMerchant += (kc.bulkOnly()==1 && kc.inMerchant()==1 ? 1 : 0);
			}
			generateLists();
			hasBoth = totalCt - (bulkOnly+weeklyOnly);
			KeheDataCompare.log("Total Products            : "+ totalCt);
			KeheDataCompare.log("Bulk Uploader only        : "+ bulkOnly);
			KeheDataCompare.log("Weekly Only Products      : "+ weeklyOnly);
			KeheDataCompare.log("Bulk Products in Merchant : " + inMerchant);
			KeheDataCompare.log("Bulk Only AND in Merchant : " + bulkOnlyInMerchant);
			// and let's show the 34 that are in bulk only and in merchant
			for(KeheContainer kc : keheDataList.values()) {
				if( kc.bulkOnly()== 1 && kc.inMerchant()==1)
					KeheDataCompare.warn(kc.toString());
			}
		} catch (IOException e) {
			//throw new Exception("IO error when trying to read error count file " + fileName, e);
		} catch (NumberFormatException e) {
			throw new Exception("Unable to parse value in error file.", e);
		}

	}

	public static void generateLists(){
		KeheDataCompare.log("Bulk Only Products : ");
		for(KeheContainer kc : keheDataList.values()){
			if(kc.bulkOnly()==1)
				KeheDataCompare.log("\t" +  kc.toString());
		}
		KeheDataCompare.log("Weekly Only Products : ");
		for(KeheContainer kc : keheDataList.values()){
			if(kc.weeklyOnly()==1)
				KeheDataCompare.log("\t" +  kc.toString());
		}
		KeheDataCompare.log("Matched Products : ");
		for(KeheContainer kc : keheDataList.values()){
			if(kc.hasBoth()==1)
				KeheDataCompare.log("\t" +  kc.toString());
		}
	}


	public static String fixupAsin(final String asin){
		String result = asin;
		while( result.length() < 8){
			result = "0"+result ;
		}
		return result ;
	}

	private static boolean productExists ( Connection conn, KeheDataItem item)  throws SQLException{
		//validate product_group
		String sql = "Select 1 From product.amazon_product Where distro = ? and asin = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, item.getDistro());
			st.setString(2, item.getItemid());
			rs = st.executeQuery();
			if ( !rs.next() ) {
				return false;
			}
			else
				return true;
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}

    private static void processRow(String[] lineCols, Connection conn)  throws Exception {
		try {
			ItemLine item = new ItemLine(lineCols);
			KeheDataCompare.log(item.getId());
			if (KeheDataCompare.isKEHE) {
				if (!item.isPhase(KeheDataCompare.phase)) {
					KeheDataCompare.log(item.getId() + "\t\t not part of phase " + KeheDataCompare.phase);
					KeheDataCompare.outOfPhase++;
					return;
				}
				if (isKeHE41 && !("Y".equals(item.getVisuality()))) {
					KeheDataCompare.log("\t\tItem not a planogram item ");
					KeheDataCompare.notPlanogram++;
					return;
				}
				KeheDataCompare.log("Add leading zeros to item\t" + item.getId());
				//App.log(item.getId() + " has " + item.categories.length + " categories." );
				// return;
				if (item.offers.equals("N")) {
					KeheDataCompare.skipInactive++;
					//App.testCount++ ;
					//return;
				}
			}
		}catch(Exception e){

		}
	}
    
	public static void main( String[] args )
    {
        if ( args.length < 1) {
        	KeheDataCompare.fatal( "Error! Please pass file path to start comparisons");
        	return;
        }
		KeheDataCompare.log( "Starting bulk KeHE compare processing.");
		Connection conn = null;
		try {
			setPaths(args[0]);  // this is the base data path which should be /somepath/kehe/comp
			conn = getDbConnectionObject("staging");  // make this a command line option 3-29
			if( args.length >= 2 ){}
				//KeheDataComp.whatToDo(args[1]);
			    // App.writeAllData = true ;
			if(!KeheDataCompare.writeAllData)
			    KeheDataCompare.log("Not running live, will be doing everything EXCEPT writing data to the DB");
			if(KeheDataCompare.verifyOnly)
				KeheDataCompare.log("Not running live, only verification of user data");
			if( args.length == 3 && !args[2].equals("")){
				KeheDataCompare.testCount = Integer.parseInt(args[2]);
			}
			buildBulkList(conn,bulkPath);
		} catch (Exception e) {
			KeheDataCompare.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			KeheDataCompare.log("Completed bulk comparisons processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				KeheDataCompare.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }

    public static void setPaths(String arg){
		keheDataPath = arg ;
		bulkPath = keheDataPath+bulkPath ;
		weeklyPath = keheDataPath+weeklyPath;
	}
    public static List<File> getListOfFiles(final String dataPath){
		return getFileList(dataPath);
	}

	public static List<File> getFileList(final String folder){
		File dir = new File(folder);
		File[] list = dir.listFiles();
		List<File> files = new ArrayList<File>();
		if(list == null ) // no files found in the list
			return files ;
		for(File f : list){
			if(f.isFile())
				files.add(f);
		}
		return files;
	}
}
