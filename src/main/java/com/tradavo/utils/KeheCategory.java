package com.tradavo.utils;

import java.util.ArrayList;
import java.util.List;

public class KeheCategory {

    private String category ;
    private List<String> merchantCats = new ArrayList<>();

    public KeheCategory(final String cat){
        this.setCategory(cat);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isEqual(final String cat){
        return this.category.equals(cat);
    }

    public List<String> getMerchantCats() {
        return merchantCats;
    }

    public void setMerchantCats(List<String> merchantCats) {
        this.merchantCats = merchantCats;
    }

    public int merchantCategories(){
        return this.merchantCats.size();
    }

    public void addMerchantCat( final String cat){
        this.merchantCats.add(cat);
    }

    public String merchantCatToString(){
        String result = "";
        for(String m : this.merchantCats){
            result += m+";";
        }
        return result.substring(0,result.lastIndexOf(";"));
    }
}
