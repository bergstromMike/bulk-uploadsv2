package com.tradavo.utils;

import com.opencsv.CSVReader;
import com.tradavo.utils.database.DBConnector;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 *
 */
public class AmuseMintsApp
{
	public static Logger logger = LogManager.getLogger(AmuseMintsApp.class) ;
	public static Logger dLog = LogManager.getLogger("descriptionRef");
	public static int updateCount = 0 ;
	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static int found = 0 ;
	public static int missing = 0 ;
	private static Connection getDbConnectionObject(final String type) throws Exception {
		try {
			App.log("Using " + type + " database connection.") ;
			return DBConnector.getConnection(type) ;
		} catch (SQLException ex) {
		    // handle any errors
			throw new Exception ( ex);
		}
	}
	public static void dlog(final String asin,final String distro){

		dLog.log(Level.INFO, asin+"|"+distro);
	}
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static void parseFile(String fileName, Connection conn) throws Exception {
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);
		try {
			// set up the first field in the csv file
			AmuseMintsApp.dlog("ASIN", "DISTRO");
		CSVReader reader = new CSVReader(new FileReader(fileName), ',');
		AmuseMintsApp.log("Processing product load file " + fileName);
		//List<Object> list = new ArrayList<>();
		//try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName),Charset.forName("Cp1252"))) {
			// br returns as stream and convert it into a List
			reader.readNext();
			int index = 1;
			String[] record = null;
			while((record = reader.readNext()) != null){
				try {
					if( AmuseMintsApp.testCount > 0 && index > AmuseMintsApp.testCount )
						break ;
					AmuseMintsApp.log ( "Record:"+ index);// a separator line
					// if( index > 2089 && index < 2091)
					processRow(record, conn);

				}
				catch ( Exception e) {
					AmuseMintsApp.log ( "Error Processing " + index + e.getMessage());
				}
				index++ ; 
			}
			reader.close();
			AmuseMintsApp.log("Total products processed: " + --index );
		} catch (IOException e) {
			throw new Exception("IO error when trying to read error count file " + fileName, e);
		} catch (NumberFormatException e) {
			throw new Exception("Unable to parse value in error file.", e);
		}
	}
	private final static String productUpdate = "update product.amazon_product set tradavo_cost = ?,retail_cost = ? "+
			" where asin = ? and distro = ?";
	private static int runUpdateSQL ( Connection conn, PreparedStatement st, final String asinIn,final String distros, String tCost, String rCost)  throws SQLException{
		//validate product_group
		ResultSet rs = null;
		PreparedStatement upSt = null ;
		int ct = 0;
		try {
			Double tdCost = Double.parseDouble(tCost);
			Double rdCost = Double.parseDouble(rCost);
			boolean found = false;
		    upSt = conn.prepareStatement(productUpdate);
			upSt.setDouble(1,tdCost);
			upSt.setDouble(2,rdCost);
			upSt.setString(3,asinIn);
			upSt.setString(4,distros);
			int res = writeData(upSt);
			if( res > 1 )
				AmuseMintsApp.log("Updated prices for " + asinIn + " and " + distros);
		}catch ( SQLException e ) {
			throw new SQLException ( e );
		}catch(NumberFormatException nfe){
			AmuseMintsApp.log(nfe.getMessage());
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			if ( upSt != null && !upSt.isClosed())
				upSt.close();
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		return ct ;
	}

	private static int productExists ( Connection conn, final String asin, final String distro,String tCost,String rCost)  throws SQLException{
		//validate product_group
		String sql = "Select a.asin,a.distro From product.amazon_product a " +
				"Where a.asin = ? and a.distro = ? "; // and active_product_count = 1";

		PreparedStatement st = null;
		ResultSet rs = null;
		int ct = 0;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, asin);
			st.setString( 2, distro) ;
			if( st.executeQuery() == null){
				AmuseMintsApp.error("Could not find AmuseMints product " + asin);
			}
			ct = AmuseMintsApp.runUpdateSQL(conn, st,asin,distro,tCost.substring(1),rCost.substring(1));
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();

			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		return ct ;
	}

    private static void processRow(String[] lineCols, Connection conn)  throws Exception{
		try
		{
			final String asin = lineCols[0];
			final String distro = lineCols[1];
			final String tCost = lineCols[3];
			final String rCost = lineCols[4];
			if(!(asin == null && asin.length()==0 ) ){
				AmuseMintsApp.productCounter(conn, asin, distro,tCost,rCost);
				AmuseMintsApp.log( " Product " + asin + " has written " + tCost + "/" + rCost + " to the database.");
			}
		}
		catch ( SQLException e) {
			conn.rollback();
			AmuseMintsApp.error(e.getMessage());
			throw new Exception ( e );
		}
	}

	public static int productCounter(Connection conn, String asin, String distros,String tCost,String rCost) throws SQLException{
		return AmuseMintsApp.productExists(conn, asin,distros,tCost,rCost) ;
	}

	public static int writeData(PreparedStatement st) throws SQLException {
	    if( AmuseMintsApp.writeAllData)
	        return st.executeUpdate() ;
	    else
	    	AmuseMintsApp.log("No AmuseMint change data written to database.");
	    return 0 ;
    }
    
	public static void main( String[] args )
    {
        if ( args.length < 1) {
        	AmuseMintsApp.fatal( "Error! Please pass file name to upload");
        	return;
        }
		AmuseMintsApp.log( "Starting AmuseMints processing.");
		Connection conn = null;
		try {
			//conn.setAutoCommit(false);
			conn = getDbConnectionObject("prod");
			if( args.length >= 2 && args[1].equals("live"))
			    AmuseMintsApp.writeAllData = true ;
			else
			    AmuseMintsApp.log("Not running live, will be doing everything EXCEPT writing data to the DB");
			if( args.length == 3 && !args[2].equals("")){
				AmuseMintsApp.testCount = Integer.parseInt(args[2]);
			}
			parseFile(args[0], conn);
			//conn.commit();
		} catch (Exception e) {
			AmuseMintsApp.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			AmuseMintsApp.log("Completed AmuseMints price update processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				AmuseMintsApp.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }
}
