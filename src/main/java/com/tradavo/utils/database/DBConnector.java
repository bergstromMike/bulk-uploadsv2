package com.tradavo.utils.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {
    public static Connection getConnection(final String type) throws SQLException{
        String connectionString = null ;

        if( type.equals("prod"))
            connectionString = "jdbc:mysql://merchantdb.clrihoh1vygg.us-east-1.rds.amazonaws.com:3306/users?" +
                    "user=admin&password=G0v1nd@1857";

        if( type.equals("staging"))
             connectionString = "jdbc:mysql://staging-tradavo.clrihoh1vygg.us-east-1.rds.amazonaws.com/users?" +
                "user=shankar&password=G0v1nd@1857";

        return DriverManager.getConnection(connectionString);
    }
}
