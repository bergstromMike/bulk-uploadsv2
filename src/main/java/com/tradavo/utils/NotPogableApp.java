package com.tradavo.utils;

import com.opencsv.CSVReader;
import com.tradavo.utils.database.DBConnector;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 *
 */
public class NotPogableApp
{
	public static Logger logger = LogManager.getLogger(NotPogableApp.class) ;
	public static Logger dLog = LogManager.getLogger("descriptionRef");
	public static int updateCount = 0 ;
	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static int found = 0 ;
	public static int missing = 0 ;
	private static Connection getDbConnectionObject(final String type) throws Exception {
		try {
			App.log("Using " + type + " database connection.") ;
			return DBConnector.getConnection(type) ;
		} catch (SQLException ex) {
		    // handle any errors
			throw new Exception ( ex);
		}
	}
	public static void dlog(final String asin,final String distro){

		dLog.log(Level.INFO, asin+"|"+distro);
	}
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static void parseFile(String fileName, Connection conn) throws Exception {
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);
		try {
			// set up the first field in the csv file
			NotPogableApp.dlog("ASIN", "DISTRO");
		CSVReader reader = new CSVReader(new FileReader(fileName), ',');
		NotPogableApp.log("Processing product load file " + fileName);
		//List<Object> list = new ArrayList<>();
		//try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName),Charset.forName("Cp1252"))) {
			// br returns as stream and convert it into a List
			reader.readNext();
			int index = 1;
			String[] record = null;
			while((record = reader.readNext()) != null){
				try {
					if( NotPogableApp.testCount > 0 && index > NotPogableApp.testCount )
						break ;
					NotPogableApp.log ( "Record:"+ index);// a separator line
					// if( index > 2089 && index < 2091)
					processRow(record, conn);

				}
				catch ( Exception e) {
					NotPogableApp.log ( "Error Processing " + index + e.getMessage());
				}
				index++ ; 
			}
			reader.close();
			NotPogableApp.log("Total products processed: " + --index );
		} catch (IOException e) {
			throw new Exception("IO error when trying to read error count file " + fileName, e);
		} catch (NumberFormatException e) {
			throw new Exception("Unable to parse value in error file.", e);
		}
	}

	private static int runUpdateSQL ( Connection conn, PreparedStatement st, final String asinIn,final String distros)  throws SQLException{
		//validate product_group
		ResultSet rs = null;
		PreparedStatement upSt = null ;
		int ct = 0;
		try {
			boolean found = false;
			rs = st.executeQuery();
		    upSt = conn.prepareStatement(productUpdate);
			while (rs.next()) {
				String asinItem = rs.getString("a.asin");
				String distroItem = rs.getString("a.distro");
				Long itemMasterId = rs.getLong("i.id");
				NotPogableApp.log("ASIN:" + asinItem + "     DISTRO:" + distroItem + "\tItem Id:" + itemMasterId);
				int res = writePogable(conn,upSt, itemMasterId);
				NotPogableApp.log(res >= 1 ? res+ " data item written to item master for itemMasterId = "+ itemMasterId :
						"Failed to write " + itemMasterId);
				ct++;
				found = true;
			}
			if (!found) {
				NotPogableApp.log("ASIN not found\t"+asinIn + "\t"+distros) ;
				//writeEtin(conn, "%"+asinIn,distros) ;
			}
		}catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			if ( upSt != null && !upSt.isClosed())
				upSt.close();
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		return ct ;
	}

	private static int productExists ( Connection conn, final String asin, final String distros)  throws SQLException{
		//validate product_group
		String sql = "Select a.asin,a.distro,i.id From product.amazon_product a " +
				"inner join product.item_master i on i.id = a.item_master_id " +
				"Where a.asin = ? and a.distro = ? "; // and active_product_count = 1";

		PreparedStatement st = null;
		ResultSet rs = null;
		int ct = 0;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, asin);
			st.setString( 2, distros) ;
			ct = NotPogableApp.runUpdateSQL(conn, st,asin,distros);
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();

			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		return ct ;
	}

	static final String productUpdate = "Update product.item_master set visuality_flag = 0 where id = ?";

	private static int writePogable( Connection conn, PreparedStatement st, Long itemMasterId) throws SQLException{
		st.setLong(1, itemMasterId);
		NotPogableApp.log("Writing item master record for item id\t"+itemMasterId) ;
		return writeData(st);
	}


    private static void processRow(String[] lineCols, Connection conn)  throws Exception{
		try
		{
			final String asin = lineCols[0];
			final String distro = lineCols[1];
			if(!(asin == null && asin.length()==0 ) ){
				NotPogableApp.log( " Product " + asin + " has " + NotPogableApp.productCounter(conn, asin, distro) + " amazon product records.");
			}
		}
		catch ( SQLException e) {
			conn.rollback();
			NotPogableApp.error(e.getMessage());
			throw new Exception ( e );
		}
	}

	public static int productCounter(Connection conn, String asin, String distros) throws SQLException{
		return NotPogableApp.productExists(conn, asin,distros) ;
	}

	public static int writeData(PreparedStatement st) throws SQLException {
	    if( NotPogableApp.writeAllData)
	        return st.executeUpdate() ;
	    else
	    	NotPogableApp.log("No not-Pogable change data written to database.");
	    return 0 ;
    }
    
	public static void main( String[] args )
    {
        if ( args.length < 1) {
        	NotPogableApp.fatal( "Error! Please pass file name to upload");
        	return;
        }
		NotPogableApp.log( "Starting Not Pogable processing.");
		Connection conn = null;
		try {
			//conn.setAutoCommit(false);
			conn = getDbConnectionObject("prod");
			if( args.length >= 2 && args[1].equals("live"))
			    NotPogableApp.writeAllData = true ;
			else
			    NotPogableApp.log("Not running live, will be doing everything EXCEPT writing data to the DB");
			if( args.length == 3 && !args[2].equals("")){
				NotPogableApp.testCount = Integer.parseInt(args[2]);
			}
			parseFile(args[0], conn);
			//conn.commit();
		} catch (Exception e) {
			NotPogableApp.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			NotPogableApp.log("Completed Not Pogable processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				NotPogableApp.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }
}
