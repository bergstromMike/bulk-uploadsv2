package com.tradavo.utils;

import com.opencsv.CSVReader;
import com.tradavo.utils.database.DBConnector;
import com.tradavo.utils.kehe.KeheAccount;
import com.tradavo.utils.kehe.KeheAccountData;
import com.tradavo.utils.lineitems.kehe.KeheLineItem;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 *
 */
public class KeheZipcodes
{
	public static Logger logger = LogManager.getLogger(KeheZipcodes.class) ;
	public static int updateCount = 0 ;
	public static int differenceCount = 0;
	public static int productMapUpdates = 0 ;
	public static boolean writeAllData = false ;  // the second parameter will choose the write data status
	public static int testCount = 0 ;
	public static boolean verifyOnly = false ;
	public static boolean compareLists = false ;
	public static boolean saveZipCodes = false ;
	private static Connection getDbConnectionObject(final String type) throws Exception {
			try {
				KeheZipcodes.log("Using " + type + " database connection.") ;
				return DBConnector.getConnection(type) ;
			} catch (SQLException ex) {
				// handle any errors
				throw new Exception ( ex);
			}
	}
	private static HashMap<String,KeheLineItem> eastList ;
	private static HashMap<String,KeheLineItem> westList ;
	public static boolean bWest = false ;
	public static void log(final String logMsg){
		logger.log(Level.INFO, logMsg);
	}
	public static void warn(final String logMsg){
		logger.log(Level.WARN, logMsg);
	}
	public static void error(final String logMsg){
		logger.log(Level.ERROR, logMsg);
	}
	public static void fatal(final String logMsg){
		logger.fatal(logMsg);
	}

	public static HashMap<String, KeheLineItem> parseFile(String fileName) throws Exception {
		File f  = new File(fileName);
		if ( !f.exists())
			throw new Exception ( "File not found " + fileName);
		KeheZipcodes.bWest = fileName.contains("West");
		HashMap<String, KeheLineItem> zipList = new HashMap<String, KeheLineItem>();
		try {
			CSVReader reader = new CSVReader(new FileReader(fileName), ',');
			KeheZipcodes.log("Processing product load file " + fileName);
			reader.readNext();
			int index = 1;
			String[] record = null;
			int  ambOnly = 0 ;
			while ((record = reader.readNext()) != null) {
				KeheLineItem line = new KeheLineItem(record);
				line.bWest = KeheZipcodes.bWest ;
				zipList.put(line.zipcode,line);
				if( !line.bPerishable ) {
					ambOnly++ ;
				}
				log(line.getId());
				log("\tDistro for this zipcode:" + line.getDistro());
				List<String> pgs = line.getProductGroups();  // just the west
				for(String s : pgs ){
					log("\tProduct Group for this zipcode:" + s);
				}
			}
			reader.close();
			//KeheZipcodes.processAccounts(conn, zipList);
			// now find all the accounts for each zipcode and state
			log("Ambient Only is " + ambOnly);
			log("Zipcode count is " + zipList.size());
		}catch(Exception e){
			error(e.getMessage());
		}
		return zipList;
	}

	public static void processAccounts(Connection conn, HashMap<String,KeheLineItem> map){
		KeheAccountData acctData = new KeheAccountData(conn);
		try {
			List<KeheLineItem> list = new ArrayList<>(map.values());
			for(KeheLineItem k : list){
				if(!k.bWriteData) {
					KeheZipcodes.warn("\tSkipping " + k.getId());
					continue;
				}
				List<KeheAccount> acctList = acctData.getZipCodeAccounts(k.zipcode, k.state);
				if (acctList.size() > 0) {
					KeheZipcodes.log(k.getId());
					List<String> pgs = k.getProductGroups();
					for (KeheAccount kk : acctList) {
						//KeheZipcodes.log("\t" + kk.info());
						KeheZipcodes.log("\t\tAccount " + kk.getAccount() + " will support " + pgs);
						for (String d : pgs) {
							if (!validateExists(conn, kk.getAccount(), d)) {
								KeheZipcodes.log("\t\tWriting product map entry for " + kk.getAccount() + "/" + d);
								updateProductGroup(conn, kk.getAccount(), d);
								productMapUpdates++;
							}
						}
					}
				} else
					KeheZipcodes.log("No accounts in " + k.zipcode + "," + k.state);
			}
		}catch(Exception se){
			KeheZipcodes.error(se.getMessage());
		}
	}

	public static List<String> findOrphans(Connection conn) throws SQLException{
		List<String> accounts = KeheAccountData.getActiveAccounts(conn);
		List<String> orphans = new ArrayList<>();
		int iCt = 0;
		for( String acct : accounts){
			// if there are account records exist for any of the product groups in KeHE then this account doesn't
			// get listed here
			KeheZipcodes.log("Processing account with counter " + iCt++);
			String sql = keheAccountStatus  ;
			PreparedStatement st = null;
			ResultSet rs = null;
			try
			{
				st = conn.prepareStatement(sql);
				st.setString(1, acct);
				rs = st.executeQuery();
				if ( rs.next()==false ) {
					orphans.add(acct);
					KeheZipcodes.error ( "No existing KeHE product map entry for " + acct);
				}
			}
			catch ( SQLException e ) {
				throw new SQLException ( e );
			}
			finally {
				if ( st != null && !st.isClosed())
					st.close();

				if ( rs != null && !rs.isClosed())
					rs.close();
			}
		}
		return orphans ;
	}

	public final static String orphansSQL = "select a.account, a.accountname, aa.ship_zip, aa.ship_state from users.accounts a " +
	"left join users.account_address aa on aa.account = a.account "+
	"where a.active >= 1  and a.account = ?";
	private static void processOrphans(Connection conn, List<String> orphans, HashMap<String,KeheLineItem> zipList) throws SQLException{
		KeheZipcodes.log("There are " + orphans.size() + " orphan accounts");
		for( String acct : orphans){
			// if there are account records exist for any of the product groups in KeHE then this account doesn't
			// get listed here
			String sql = orphansSQL  ;
			PreparedStatement st = null;
			ResultSet rs = null;
			try
			{
				st = conn.prepareStatement(sql);
				st.setString(1, acct);
				rs = st.executeQuery();
				while( rs.next() ) {
					String acctName = rs.getString("a.accountname");
					String zip = rs.getString("aa.ship_zip");
					String state = rs.getString("aa.ship_state");
					KeheLineItem khZip = zipList.get(zip);
					if( khZip == null )
						KeheZipcodes.log(","+acct+","+acctName+","+zip+","+state);
					else
						KeheZipcodes.error(","+acct+","+acctName+","+zip+",A:"+state+",F:"+khZip.state);
				}
			}
			catch ( SQLException e ) {
				throw new SQLException ( e );
			}
			finally {
				if ( st != null && !st.isClosed())
					st.close();

				if ( rs != null && !rs.isClosed())
					rs.close();
			}
		}
	}

	private static String keheAccountStatus = "select * from product.cust_product_map where account = ? and product_group in ('khep','khea','khca','khcp')";
	private static String accountProdMapExists = "Select 1 from product.cust_product_map where account = ? and product_group = ?";
	private static boolean validateExists ( Connection conn, String account, String group)  throws SQLException{
		//validate product_group
		//if(writeAllData)
		//	return true;
		String sql = accountProdMapExists ;
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = conn.prepareStatement(sql);
			st.setString(1, account);
			st.setString(2, group);
			rs = st.executeQuery();
			if ( rs.next()==false ) {
				KeheZipcodes.error ( "No existing product map for " + account + "/"+ group);
				return false;
			}
			else {
				//KeheZipcodes.log("Cust Product Map entry exists for " + account + "/"+ group);
				return true;
			}
		}
		catch ( SQLException e ) {
			throw new SQLException ( e );
		}
		finally {
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
	}

	static final String saveZipCodeSQL = "replace into product.kehe_zip_codes (zip_code,state,distro,product_group) values (?,?,?,?);";
	private static void saveZipCodeList(Connection conn, List<KeheLineItem> list){
		String sql = saveZipCodeSQL;
		PreparedStatement st = null;

		int ct = 0 ;
		for(KeheLineItem k : list){    // all the zip codes
			log("Saving zip code data to merchant.");
			log("\t" + k.getId());
			try {
				if(testCount > 0 && ct > testCount)
					return ;
				List<String> prodgroups = k.getProductGroups();
				for( String s : prodgroups) {   // however many product groups we have to
					st = conn.prepareStatement(sql);
					st.setString(1, k.zipcode);
					st.setString(2, k.state);
					st.setString(3, k.getDistro());
					st.setString(4, s);
					writeData(st); // .executeUpdate();
					if (st != null && !st.isClosed())
						st.close();
				}
			}
			catch ( SQLException e) {
				log( e.getMessage());
			}
			finally {
				ct++ ;
			}
		}
	}

	static final String custMapUpdate = "replace into product.cust_product_map ( account,product_group ) Values (?,?) ";
	private static void updateProductGroup(Connection conn, final String account, final String prodGroup) throws Exception{
		String sql = custMapUpdate;

		//KeheZipcodes.log("SQL for update: " + sql);
		PreparedStatement st = null;

		ResultSet rs = null;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1,account);
			st.setString( 2,prodGroup);
			writeData(st); // .executeUpdate();
		}
		catch ( SQLException e) {
			throw new Exception ( e);
		}
		finally {
			updateCount++ ;
			if ( st != null && !st.isClosed())
				st.close();
			
			if ( rs != null && !rs.isClosed())
				rs.close();
		}
		
	}

	public static void compareKeheZipCodeLists(String eastFile) throws Exception{
		 // get the first list from initialFile
		KeheZipcodes.eastList = parseFile(eastFile);
		// and now get a secondary list after modifying the initialFile value subbing West for East
		String westFile = eastFile.replace("East","West");
		KeheZipcodes.westList = parseFile(westFile);
		KeheZipcodes.log("East list has " + eastList.size() + " records");
		KeheZipcodes.log("West list has " + westList.size() + " records");
		// loop the east list and find all the zip codes that are shared between the two
		KeheZipcodes.log("Shared zip codes");
		List<KeheLineItem> list = new ArrayList<>(westList.values());
		for(KeheLineItem k : list){
			KeheLineItem kk = eastList.get(k.zipcode);
			if(kk != null){
				kk.writeData(k);
				// fix up the logging here
				KeheZipcodes.log("Zip Code: "+kk.zipcode+"\tState: "+ kk.state);
				//KeheZipcodes.log(",West:"+k.getId()+",East:"+kk.getId());
				KeheLineItem kke = eastList.get(k.zipcode);
				KeheLineItem kkw = westList.get(k.zipcode);
				KeheZipcodes.log("\tWest:"+kkw.getDetails());
				KeheZipcodes.log("\tEast:"+kke.getDetails());
			}
		}
	}

	public static void writeData(PreparedStatement st) throws SQLException {
	    if( KeheZipcodes.writeAllData) {
			if (st.executeUpdate() == 0)
				KeheZipcodes.error("\t\tError writing data for this entry.");
		}//else
	    //	KeheZipcodes.log("\t"+"No actual data written to database.");
    }
    
	public static void main( String[] args )
    {
        if ( args.length < 1) {
        	KeheZipcodes.fatal( "Error! Please pass file name to upload");
        	return;
        }
		KeheZipcodes.log( "Starting KeHE request processing.");
		Connection conn = null;
		try {
			if (args.length >= 2)
				KeheZipcodes.whatToDo(args[1]);
			// always load both sets of zip codes, do stuff to make sure that you only post the correct ones.
			compareKeheZipCodeLists(args[0]);
			if(KeheZipcodes.compareLists)
				return ;
			else {
				conn = getDbConnectionObject("prod");  // make this a command line option 3-29
				// App.writeAllData = true ;
				if (!KeheZipcodes.writeAllData)
					KeheZipcodes.log("Not running live, will be doing everything EXCEPT writing data to the DB");
				if (KeheZipcodes.verifyOnly)
					KeheZipcodes.log("Not running live, only verification of user data");
				if (args.length == 3 && !args[2].equals("")) {
					KeheZipcodes.testCount = Integer.parseInt(args[2]);
				}
				HashMap<String, KeheLineItem> zipList = westList ; //KeheZipcodes.bWest ? westList : eastList ;//parseFile(args[0]);
				if (KeheZipcodes.verifyOnly) {
					List<String> orphans = findOrphans(conn);
					processOrphans(conn, orphans, zipList);
				}else if(KeheZipcodes.saveZipCodes){
					saveZipCodeList(conn, westList.values().stream().collect(Collectors.toList()));
					saveZipCodeList(conn, eastList.values().stream().collect(Collectors.toList()));
				} else {
					processAccounts(conn, westList);
					processAccounts(conn, eastList);
				}
			}
		} catch (Exception e) {
			KeheZipcodes.fatal(e.getMessage());
			e.printStackTrace();
		}
		finally {
			KeheZipcodes.log("Accounts updated with new product map entries: " + productMapUpdates) ;
			KeheZipcodes.log("Completed KeHE request processing.") ;
			try {
				if ( conn != null && !conn.isClosed() )
					conn.close() ;
			} catch (SQLException e) {
				KeheZipcodes.error(e.getMessage());
				e.printStackTrace();
			}
		}
    }
    private static void whatToDo( final String what){
		if( what.equals("live"))
			KeheZipcodes.writeAllData = true ;
		if( what.equals("verify"))
			KeheZipcodes.verifyOnly = true ;
		if( what.equals("comp"))
			KeheZipcodes.compareLists = true ;
		if( what.equals("save")) {
			KeheZipcodes.saveZipCodes = true;
			KeheZipcodes.writeAllData = true ;  // save catches itself
		}
	}
}
